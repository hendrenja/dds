import json
import os
import sys
import errno

###
### Default Foo class properties
###
readerHeader =   '#include <oasys/core/dds/connext/factory/foo_reader.h>'
writerHeader =   '#include <oasys/core/dds/connext/factory/foo_writer.h>'
topicName =      'DEFAULT_TOPIC_NAME      = "Foo";'
qosProfile =     'DEFAULT_QOS_PROFILE     = "OASYS::BestEffort";'
qosParticipant = 'DEFAULT_PARTICIPANT_QOS = "OASYS::BestEffort";'
qosTopic =       'DEFAULT_TOPIC_QOS       = "OASYS::BestEffort";'
qosPubSub =      'DEFAULT_PUBSUB_QOS      = "OASYS::BestEffort";'
qosEntity =      'DEFAULT_ENTITY_QOS      = "OASYS::BestEffort";'
headerGuard =    "_FOO_"
model =          "foo/Foo.hpp"
modelClass =     "CFoo"
name =           "<Foo>"
filename =       "foo"
namespace =      "namespace foo"
usingNamespace = "using namespace oasys::core::interfaces::foo;"

idlDir = ''
fooReaderHeader = ''
fooReaderSource = ''
fooWriterHeader = ''
fooWriterSource = ''

def fullyQualifySource(srcDir):
    global fooReaderHeader
    fooReaderHeader = srcDir + '/../connext/factory/include/foo_reader.h'
    global fooReaderSource
    fooReaderSource = srcDir + '/../connext/factory/src/foo_reader.cpp'
    global fooWriterHeader
    fooWriterHeader = srcDir + '/../connext/factory/include/foo_writer.h'
    global fooWriterSource
    fooWriterSource = srcDir + '/../connext/factory/src/foo_reader.cpp'

class Template:
    targetDir = ''
    namespace = ''
    headerGuard = ''
    model = ''
    modelClass = ''
    name = ''
    filename = ''
    topicName = ''
    qosProfile = ''
    qosParticipant = ''
    qosTopic = ''
    qosPubSub = ''
    qosEntity = ''

def mkdir_p(path):
    try:
        os.makedirs(path, 0755)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def execReplacements(template, sourceFile):
    #Prepare Targets
    targetHeaderGuard = headerGuard.replace(headerGuard, "_"+template.headerGuard+"_")
    targetNamespace = namespace.replace(namespace, "namespace "+template.namespace)
    targetModelClass = modelClass.replace(modelClass, "C"+template.modelClass)
    targetUsingNamespace = usingNamespace.replace(usingNamespace, "using namespace oasys::core::interfaces::"+template.namespace+";")
    targetReaderHeader = readerHeader.replace(readerHeader,
                            "#include <oasys/core/dds/connext/interfaces/"+template.namespace+"/"+template.filename+"_reader.hpp>")
    targetWriterHeader = writerHeader.replace(writerHeader,
                            "#include <oasys/core/dds/connext/interfaces/"+template.namespace+"/"+template.filename+"_writer.hpp>")
    targetName = template.name.replace(name, "<"+template.name+">")
    newTopicName = 'DEFAULT_TOPIC_NAME      = "'+template.topicName+'";'
    targetTopicName = topicName.replace(topicName, newTopicName)
    targetQosProfile = qosProfile.replace(qosProfile, 'DEFAULT_QOS_PROFILE     = "'+template.qosProfile+'";')
    targetQosQosParticipant = qosParticipant.replace(qosParticipant, 'DEFAULT_PARTICIPANT_QOS     = "'+template.qosParticipant+'";')
    targetQosTopic = qosTopic.replace(qosTopic, 'DEFAULT_TOPIC_QOS     = "'+template.qosTopic+'";')
    targetQosPubSub = qosPubSub.replace(qosPubSub, 'DEFAULT_PUBSUB_QOS     = "'+template.qosPubSub+'";')
    targetQosEntity = qosEntity.replace(qosEntity, 'DEFAULT_ENTITY_QOS     = "'+template.qosEntity+'";')


    ## DO REPLACEMENTS
    sourceFile = sourceFile.replace(topicName, targetTopicName)
    sourceFile = sourceFile.replace(qosProfile, targetQosProfile)
    sourceFile = sourceFile.replace(qosParticipant, targetQosQosParticipant)
    sourceFile = sourceFile.replace(qosTopic, targetQosTopic)
    sourceFile = sourceFile.replace(qosPubSub, targetQosPubSub)
    sourceFile = sourceFile.replace(qosEntity, targetQosEntity)
    sourceFile = sourceFile.replace(headerGuard, targetHeaderGuard)
    sourceFile = sourceFile.replace(readerHeader, targetReaderHeader)
    sourceFile = sourceFile.replace(writerHeader, targetWriterHeader)
    sourceFile = sourceFile.replace(modelClass, targetModelClass)
    sourceFile = sourceFile.replace(namespace, targetNamespace)
    sourceFile = sourceFile.replace(usingNamespace, targetUsingNamespace)
    sourceFile = sourceFile.replace(name, targetName)
    sourceFile = sourceFile.replace(model, template.model)

    return sourceFile

def writeSource(template):
    directory = idlDir + '/../connext/interfaces/' + template.targetDir + \
                     '/src/'
    mkdir_p(directory)
    readerFile = directory + template.filename + '_reader.cxx'
    writerFile = directory + template.filename + '_writer.cxx'
    print "Create Reader Header: " + readerFile
    print "Create Writer Header: " + writerFile
    sourceReader = fooReaderSource
    sourceWriter = fooWriterSource
    sourceReaderFile = ''
    sourceWriterFile = ''
    with open(sourceReader) as sourceFile:
        sourceReaderFile = sourceFile.read()

    with open(sourceWriter) as sourceFile:
        sourceWriterFile = sourceFile.read()


    sourceReaderFile = execReplacements(template, sourceReaderFile)
    sourceWriterFile = execReplacements(template, sourceWriterFile)

    ### Write Files
    with open(readerFile, "w") as outFile:
        outFile.write(sourceReaderFile)

    with open(writerFile, "w") as outFile:
        outFile.write(sourceWriterFile)


def writeHeader(template):
    directory = idlDir + '/../connext/interfaces/' + template.targetDir + \
                     '/include/'
    print "Create directory " + directory
    mkdir_p(directory)
    readerFile = directory + template.filename + '_reader.hpp'
    writerFile = directory + template.filename + '_writer.hpp'
    print "Create Reader Source: " + readerFile
    print "Create Writer Source: " + writerFile
    headerReader = fooReaderHeader
    headerWriter = fooWriterHeader
    headerReaderFile = ''
    headerWriterFile = ''
    with open(headerReader) as headerFile:
        headerReaderFile = headerFile.read()

    with open(headerWriter) as headerFile:
        headerWriterFile = headerFile.read()

    headerReaderFile = execReplacements(template, headerReaderFile)
    headerWriterFile = execReplacements(template, headerWriterFile)

    ### Write Files
    with open(readerFile, "w") as outFile:
        outFile.write(headerReaderFile)

    with open(writerFile, "w") as outFile:
        outFile.write(headerWriterFile)

def loadFile(configFile):
    print "\nConfig file: ", str(configFile)
    jsonConfig = {}
    with open(configFile) as jsonConfigFile:
        configStr = jsonConfigFile.read()
        jsonConfig = json.loads(configStr)

    interfaces = jsonConfig["interfaces"]
    for interface in interfaces:
        template = Template()
        template.targetDir = jsonConfig['targetDir']
        template.namespace = jsonConfig['namespace']
        template.headerGuard = interface['headerGuard']
        template.model = interface['model']
        template.modelClass = interface['modelClass']
        template.name = interface['name']
        template.filename = interface['filename']
        template.topicName = interface['TopicName']
        template.qosProfile = interface['QosProfile']
        template.qosParticipant = interface['QosParticipant']
        template.qosTopic = interface['QosTopic']
        template.qosPubSub = interface['QosPubSub']
        template.qosEntity = interface['QosEntity']
        writeHeader(template)
        writeSource(template)

def main(argv):
    numArg = len(argv)
    if numArg != 2:
        print "\n\nUsage: connext.py <config.json>"
        print "Received Args: ", str(argv)
        sys.exit(-1)

    global idlDir
    idlDir = os.path.dirname(os.path.realpath(argv[0]))
    fullyQualifySource(idlDir)
    loadFile(os.getcwd() + "/" + argv[1])

if __name__ == "__main__":
    main(sys.argv)
