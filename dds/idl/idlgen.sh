#!/bin/bash

usage ()
{
    echo "Usage: idlgen -n <type> -h <home>"
    echo "type: all | foo"
    echo "home: home directory path"
    exit
}

if [ "$#" -ne 4 ]
then
    usage
fi

createDir()
{
    if [ ! -d $1 ] ;
    then
        ## Create Directory
        mkdir -p $1
    fi
}

INC_DIR=`pwd`
HOME_DIR=$4
DDS_DIR=${HOME_DIR}/core/dds
CONNEXT_DIR="${DDS_DIR}/connext/models"
OSPL_DIR="${DDS_DIR}/ospl/models"
CONNEXT_OPT="-namespace -language C++11 -create typefiles -update typefiles"

echo "Home DIR = ${HOME_DIR}"

FooConnext() {
    IDL_DIR=${DDS_DIR}/idl/foo
    TARGET=${CONNEXT_DIR}/foo
    TARGET_INC=${TARGET}/include
    TARGET_SRC=${TARGET}/src

    ${NDDSHOME}/bin/rtiddsgen ${CONNEXT_OPT} -inputIdl ${IDL_DIR}/Foo.idl

    createDir ${TARGET_INC}
    createDir ${TARGET_SRC}

    mv ${IDL_DIR}/*.h* ${TARGET_INC}
    mv ${IDL_DIR}/*.c* ${TARGET_SRC}
}

FooOspl() {
    PROJECT_DIR=${DDS_DIR}/idl
    TARGET=${OSPL_DIR}/foo
    TARGET_INC=${TARGET}/include
    TARGET_SRC=${TARGET}/src

    idlpp -S -l isocpp2 -I ${INC_DIR} ${PROJECT_DIR}/Foo.idl

    createDir ${TARGET_INC}
    createDir ${TARGET_SRC}

    mv *.h* ${TARGET_INC}
    mv *.c* ${TARGET_SRC}
}

Foo() {
    FooConnext
}

if [ $2 = "all" ]
then
    Foo
fi

if [ $2 = "foo" ]
then
    Foo
fi
