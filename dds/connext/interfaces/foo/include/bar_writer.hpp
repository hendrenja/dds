#ifndef __OASYS_CORE_DDS_CONNEXT_FACTORY_BAR_WRITER_H__
#define __OASYS_CORE_DDS_CONNEXT_FACTORY_BAR_WRITER_H__

#include <oasys/core/dds/connext/factory/factory.h>
#include <oasys/core/dds/connext/factory/waitset_thread.h>
#include <oasys/core/common/common.h>
#include <oasys/core/dds/connext/models/foo/Bar.hpp>

namespace oasys
{
namespace core
{
namespace interfaces
{
namespace foo
{

class CBarWriter
{
public:
    /**
     * \defgroup Type Definitions
     * @{
     */
    /// \brief Public type definitions for DDS Writer
    typedef oasys::core::common::TDataDelegate<dds::core::status::OfferedDeadlineMissedStatus> DeadlineDelegate;
    typedef oasys::core::common::TDataDelegate<dds::core::status::LivelinessLostStatus> LivelinessDelegate;
    typedef oasys::core::common::TDataDelegate<dds::core::status::PublicationMatchedStatus> PublicationMatchedDelegate;

    /**
     * \defgroup Data Writers
     * @{
     */
    /// \brief Methods for writing Foo samples
    virtual dds::core::InstanceHandle RegisterInstance(Foo &data);
    virtual bool UnregisterInstance(dds::core::InstanceHandle &handle, bool dispose = true);
    virtual bool Write(Foo &data);
    virtual bool Write(Foo &data, dds::core::InstanceHandle &handle);
    /**@}*/

    /**
     * \defgroup Register Async Interface Status Handlers
     * @{
     */
    ///
    /// \brief Register delegates for receiving DDS interface status updates
    ///
    bool RegisterDeadlineSubscriber(DeadlineDelegate &delegate);
    bool UnregisterDeadlineSubscriber(oasys::core::common::OwnerSharedPtr pOwner);
    bool RegisterLivelinessSubscriber(LivelinessDelegate &delegate);
    bool UnregisterLivelinessSubscriber(oasys::core::common::OwnerSharedPtr pOwner);
    bool RegisterPublicationMatchedSubscriber(PublicationMatchedDelegate &delegate);
    bool UnregisterPublicationMatchedSubscriber(oasys::core::common::OwnerSharedPtr pOwner);
    /**@}*/


    /**
     * \defgroup Configuration
     * @{
     */
    ///
    /// \brief Configure DDS Writer
    ///
    void SetTopicName(const std::string &name);
    void SetQosProfile(const std::string &qos);
    void SetParticipantQos(const std::string &qos);
    void SetPublisherQos(const std::string &qos);
    void SetTopicQos(const std::string &qos);
    void SetWriterQos(const std::string &qos);

    virtual bool Initialize();
    bool Destroy();

    CBarWriter();
    CBarWriter(uint32_t domainId);
    CBarWriter(const std::string topicName);
    CBarWriter(uint32_t domainId,
               const std::string topicName);
    CBarWriter(const uint32_t domainId,
               const std::string topicName,
               const std::string &qosProfile,
               const std::string &participantQos,
               const std::string &subscriberQos,
               const std::string &topicQos,
               const std::string &writerQos);
    virtual ~CBarWriter();
    /**@}*/

protected:
    virtual void StatusConditionUpdate(CFunctor * const pFunctor);
    virtual void ProcessDeadline();
    virtual void ProcessLiveliness();
    virtual void ProcessPublicationMatched();

    bool CreateTopic(const std::string topicName,
                     const std::string &topicQos);
    bool DeleteTopic();
    bool CreateWriter(const std::string &qosProfile);
    bool DeleteWriter();

private:
    typedef std::lock_guard<std::mutex> LockGuard;
    typedef std::vector<DeadlineDelegate> DeadlineVector;
    typedef typename DeadlineVector::iterator DeadlineIt;
    typedef std::vector<LivelinessDelegate> LivelinessVector;
    typedef typename LivelinessVector::iterator LivelinessIt;
    typedef std::vector<PublicationMatchedDelegate> PublicationVector;
    typedef typename PublicationVector::iterator PublicationIt;
    typedef std::vector<CFunctor*> FunctorVector;
    typedef FunctorVector::iterator FunctorIt;
    typedef dds::topic::TopicBar Topic;
    typedef dds::pub::DataWriterBar Writer;

    virtual void EnableConditionDeadline();
    virtual void EnableConditionLiveliness();
    virtual void EnableConditionPublicationMatched();
    void DisableConditionDeadline();
    void DisableConditionLiveliness();
    void DisableConditionPublicationMatched();

    FunctorIt FindFunctor(CFunctor *pFunctor);
    FunctorIt FindFunctor(dds::core::cond::StatusCondition &condition);
    bool DeleteFunctor(CFunctor *pFunctor);
    bool DeleteFunctor(dds::core::cond::StatusCondition &condition);

    uint32_t                            m_domainId;
    std::string                         m_topicName;
    std::string                         m_qosProfile;
    std::string                         m_participantQos;
    std::string                         m_publisherQos;
    std::string                         m_topicQos;
    std::string                         m_writerQos;

    CDdsFactory                         *m_pFactory;
    CWaitsetThread                      *m_pWaitsetThread;
    Topic                               m_topic;
    Writer                              m_writer;
    dds::pub::Publisher                 m_publisher;
    dds::domain::DomainParticipant      m_participant;

    dds::core::cond::StatusCondition    m_deadlineCondition;
    dds::core::cond::StatusCondition    m_livelinessCondition;
    dds::core::cond::StatusCondition    m_publicationCondition;

    DeadlineVector                      m_deadlineVector;
    LivelinessVector                    m_livelinessVector;
    PublicationVector                   m_publicationVector;
    FunctorVector                       m_functorVector;

    std::mutex                          m_mutexDelegate;
    std::mutex                          m_mutexMember;
    std::mutex                          m_mutexPublisher;
    std::mutex                          m_mutexTopic;
    std::mutex                          m_mutexWriter;
};

}
}
}
}

#endif //__OASYS_CORE_DDS_CONNEXT_FACTORY_BAR_WRITER_H__
