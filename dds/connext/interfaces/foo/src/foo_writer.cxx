#include <oasys/core/dds/connext/interfaces/foo/foo_reader.hpp>
#include <oasys/core/dds/connext/factory/functor.h>
#include <exception.h>
#include <factory_macros.h>
#include <oasys/core/dds/connext/factory/ndds/hpp/rti/config/Logger.hpp>

const std::string DEFAULT_QOS_URI = "/usr/local/etc/corto/1.1/oasys/core/dds/connext/factory/qos.xml";

const uint32_t      DEFAULT_DOMAIN_ID       = 0;
const std::string   DEFAULT_TOPIC_NAME      = "Foo";
const std::string   DEFAULT_QOS_PROFILE     = "OASYS::BestEffort";
const std::string   DEFAULT_PARTICIPANT_QOS     = "OASYS::BestEffort";
const std::string   DEFAULT_PUBSUB_QOS     = "OASYS::BestEffort";
const std::string   DEFAULT_TOPIC_QOS     = "OASYS::BestEffort";
const std::string   DEFAULT_ENTITY_QOS     = "OASYS::BestEffort";

using namespace oasys::core;
using namespace oasys::core::interfaces::foo;

typedef std::lock_guard<std::mutex> LockGuard;

CFooReader::SampleSeq CFooReader::Query(std::string &expression)
{
    SampleSeq retVal;

    if (m_reader.is_nil() == true)
    {
        std::string error("Query Failed: Reader null.");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }

    try
    {
        dds::sub::status::DataState state(dds::sub::status::SampleState::any(),
                                          dds::sub::status::ViewState::any(),
                                          dds::sub::status::InstanceState::alive());
        dds::sub::Query query(m_reader, expression);
        dds::sub::cond::QueryCondition condition(query, state);
        retVal = QueryWithCondition(condition);
    }
    catch (const dds::core::Exception& e)
    {
        std::string error("Query exception: ["+std::string(e.what())+"]");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }

    return retVal;
}

CFooReader::SampleSeq CFooReader::Query(std::string &expression,
                                        CFooReader::ParamVector &params)
{
    SampleSeq retVal;

    if (m_reader.is_nil() == true)
    {
        std::string error("Query Failed: Reader null.");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }

    try
    {
        dds::sub::status::DataState state(dds::sub::status::SampleState::any(),
                                          dds::sub::status::ViewState::any(),
                                          dds::sub::status::InstanceState::alive());
        dds::sub::Query query(m_reader, expression, params);
        dds::sub::cond::QueryCondition condition(query, state);
        retVal = QueryWithCondition(condition);
    }
    catch (const dds::core::Exception& e)
    {
        std::string error("Query exception: ["+std::string(e.what())+"]");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }

    return retVal;
}

CFooReader::Sample CFooReader::Read(ReadBuffer direction)
{
    return ReadWithCondition(m_aliveDataCondition, direction);
}

CFooReader::SampleSeq CFooReader::ReadSeq()
{
    return ReadWithCondition(m_aliveDataCondition, false);
}

CFooReader::Sample CFooReader::Take(ReadBuffer direction,
                                    dds::core::InstanceHandle handle)
{
    return ReadWithCondition(m_aliveDataCondition, direction, true, handle);
}

CFooReader::SampleSeq CFooReader::TakeSeq(dds::core::InstanceHandle handle)
{
    return ReadWithCondition(m_aliveDataCondition, true, handle);
}

bool CFooReader::RegisterNewDataSubscriber(DataDelegate &delegate)
{
    bool retVal = false;

    LockGuard lock(m_mutexDelegate);

    DataDelegateIt it = FindDataDelegate(delegate.GetOwnerPtr());
    if (it == m_dataDelegateVector.end())
    {
        m_dataDelegateVector.push_back(delegate);

        it = FindDataDelegate(delegate.GetOwnerPtr());
    }

    // Verify delegate was added
    if (it != m_dataDelegateVector.end())
    {
        retVal = true;
    }

    return retVal;
}

bool CFooReader::UnregisterNewDataSubscriber(common::OwnerSharedPtr pOwner)
{
    bool retVal = false;

    LockGuard lock(m_mutexDelegate);

    common::OwnerWeakPtr pOwnerWeak = pOwner;
    DataDelegateIt it = FindDataDelegate(pOwnerWeak);

    if (it != m_dataDelegateVector.end())
    {
        m_dataDelegateVector.erase(it);
        retVal = true;
    }

    return retVal;
}

bool CFooReader::RegisterInstanceNotAliveSubscriber(DataDelegate &delegate)
{
    bool retVal = false;

    LockGuard lock(m_mutexDelegate);

    DataDelegateIt it = FindNotAliveDelegate(delegate.GetOwnerPtr());
    if (it == m_dataNotAliveVector.end())
    {
        m_dataNotAliveVector.push_back(delegate);

        it = FindNotAliveDelegate(delegate.GetOwnerPtr());
    }

    // Verify delegate was added
    if (it != m_dataNotAliveVector.end())
    {
        retVal = true;
    }

    return retVal;
}

bool CFooReader::UnregisterInstanceNotAliveSubscriber(common::OwnerSharedPtr pOwner)
{
    bool retVal = false;

    LockGuard lock(m_mutexDelegate);

    common::OwnerWeakPtr pOwnerWeak = pOwner;
    DataDelegateIt it = FindNotAliveDelegate(pOwnerWeak);

    if (it != m_dataNotAliveVector.end())
    {
        m_dataNotAliveVector.erase(it);
        retVal = true;
    }

    return retVal;
}

bool CFooReader::RegisterQuery(std::string expression,
                               ParamVector &params,
                               DataSeqDelegate &delegate)
{
    bool retVal = false;

    LockGuard lock(m_mutexDelegate);

    CFunctor::Callback callback =
            [&] (CFunctor * const pFunctor)
    {
        dds::sub::cond::QueryCondition queryCondition(dds::core::null);
        if (pFunctor->GetCondition(queryCondition) == true)
        {
            std::string expression(pFunctor->GetExpression());
            ProcessQueryCondition(queryCondition, expression);
        }
    };

    if (m_reader.is_nil() == true)
    {
        std::string error("Failed to register query: Reader null");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }

    try
    {
        dds::sub::status::DataState state(dds::sub::status::SampleState::not_read(),
                                          dds::sub::status::ViewState::any(),
                                          dds::sub::status::InstanceState::alive());
        dds::sub::Query query(m_reader, expression, params);

        CFunctor *pFunctor = new CFunctor(callback, expression);
        dds::sub::cond::QueryCondition condition(query, state, *pFunctor);
        pFunctor->SetCondition(condition);
        m_functorVector.push_back(pFunctor);

        auto mapIt = m_queryMap.find(expression);
        if (mapIt == m_queryMap.end())
        {
            if (m_pWaitsetThread->RegisterCondition(pFunctor) == true)
            {
                QueryPair queryPair(condition, delegate);
                QueryVector vector;
                vector.push_back(queryPair);
                QueryMapPair mapPair(expression, vector);
                m_queryMap.insert(mapPair);
                retVal = true;
            }
            else
            {
                std::string error("Failed to register query");
                DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
                throw CDdsException(m_topicName, error);
            }
        }
        else
        {
            QueryVector vector = mapIt->second;
            QueryVectorIt delegateIt = FindQueryExpression(vector, delegate.GetOwnerPtr());
            if (delegateIt == vector.end())
            {
                if (m_pWaitsetThread->RegisterCondition(pFunctor) == true)
                {
                    QueryPair queryPair(condition, delegate);
                    vector.push_back(queryPair);
                    mapIt->second = vector;
                    retVal = true;
                }
                else
                {
                    std::string error("Failed to register query");
                    DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
                    throw CDdsException(m_topicName, error);
                }
            }
            else
            {
                /// Delegate Already Exists
                retVal = true;
            }
        }

        if ((retVal == false) && (pFunctor != nullptr))
        {
            DeleteFunctor(pFunctor);
            delete pFunctor;
            pFunctor = nullptr;
        }
    }
    catch (const dds::core::Exception& e)
    {
        std::string error("Register query failed: \n["+std::string(e.what())+"]");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }

    return retVal;
}

bool CFooReader::UnRegisterQuery(std::string &expression,
                                 common::OwnerSharedPtr pOwner)
{
    bool retVal = false;

    common::OwnerWeakPtr pOwnerWeak = pOwner;
    auto mapIt = m_queryMap.find(expression);
    if (mapIt != m_queryMap.end())
    {
        QueryVector vector = mapIt->second;
        QueryVectorIt vectorIt = FindQueryExpression(vector, pOwnerWeak);
        if (vectorIt != vector.end())
        {
            vector.erase(vectorIt);
        }

        if (vector.empty() == true)
        {
            m_queryMap.erase(mapIt);
            retVal = true;
        }
        else
        {
            mapIt->second = vector;
        }
    }

    return retVal;
}

bool CFooReader::RegisterDeadlineSubscriber(DeadlineDelegate &delegate)
{
    LockGuard lock(m_mutexDelegate);
    return RegisterSubscriber<DeadlineVector, DeadlineDelegate>(m_deadlineVector, delegate);
}

bool CFooReader::UnregisterDeadlineSubscriber(common::OwnerSharedPtr pOwner)
{
    LockGuard lock(m_mutexDelegate);
    return UnregisterSubscriber<DeadlineVector>(m_deadlineVector, pOwner);
}

bool CFooReader::RegisterLivelinessSubscriber(LivelinessDelegate &delegate)
{
    LockGuard lock(m_mutexDelegate);
    return RegisterSubscriber<LivelinessVector, LivelinessDelegate>(m_livelinessVector, delegate);
}

bool CFooReader::UnregisterLivelinessSubscriber(common::OwnerSharedPtr pOwner)
{
    LockGuard lock(m_mutexDelegate);
    return UnregisterSubscriber<LivelinessVector>(m_livelinessVector, pOwner);
}

bool CFooReader::RegisterSampleRejectedSubscriber(SampleRejectedDelegate &delegate)
{
    LockGuard lock(m_mutexDelegate);
    return RegisterSubscriber<SampleRejectedVector, SampleRejectedDelegate>(m_sampleRejectedVector, delegate);
}

bool CFooReader::UnregisterSampleRejectedSubscriber(common::OwnerSharedPtr pOwner)
{
    LockGuard lock(m_mutexDelegate);
    return UnregisterSubscriber<SampleRejectedVector>(m_sampleRejectedVector, pOwner);
}

bool CFooReader::RegisterSubscriptionMatchedSubscriber(SubscriptionMatchedDelegate &delegate)
{
    LockGuard lock(m_mutexDelegate);
    return RegisterSubscriber<SubscriptionVector, SubscriptionMatchedDelegate>(m_subscriptionVector, delegate);
}

bool CFooReader::UnregisterSubscriptionMatchedSubscriber(common::OwnerSharedPtr pOwner)
{
    LockGuard lock(m_mutexDelegate);
    return UnregisterSubscriber<SubscriptionVector>(m_subscriptionVector, pOwner);
}

void CFooReader::SetTopicName(std::string name)
{
    LockGuard lock(m_mutexMember);
    m_topicName = name;
}

void CFooReader::SetQosProfile(std::string qos)
{
    LockGuard lock(m_mutexMember);
    m_qosProfile = qos;
}

void CFooReader::SetParticipantQos(std::string qos)
{
    LockGuard lock(m_mutexMember);
    m_participantQos = qos;
}

void CFooReader::SetSubscriberQos(std::string qos)
{
    LockGuard lock(m_mutexMember);
    m_subscriberQos = qos;
}

void CFooReader::SetTopicQos(std::string qos)
{
    LockGuard lock(m_mutexMember);
    m_topicQos = qos;
}

void CFooReader::SetReaderQos(std::string qos)
{
    LockGuard lock(m_mutexMember);
    m_readerQos = qos;
}

bool CFooReader::Initialize()
{
    bool retVal = false;

    LockGuard lockGuard(m_mutexMember);

    if (m_pFactory == nullptr)
    {
        m_pFactory = DdsFactory::GetInstance();
        if (m_pFactory != nullptr)
        {
            ///TODO Get URI FROM ENV
            if (m_pFactory->Initialize(DEFAULT_QOS_URI) == false)
            {
                std::string error("Failed to initialize factory with [" + DEFAULT_QOS_URI + "] xml.");
                DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
                return false;
            }
        }
    }

    if (m_pWaitsetThread == nullptr)
    {
        m_pWaitsetThread = new CWaitsetThread(m_topicName);
    }

    bool participantValid = !m_participant.is_nil();
    if (participantValid == false)
    {
        participantValid = m_pFactory->CreateDomainParticipant(m_participant,
                                                               m_domainId,
                                                               m_participantQos);
    }

    bool subscriberValid = !m_subscriber.is_nil();
    if ((subscriberValid == false) && (participantValid == true))
    {
        subscriberValid = m_pFactory->CreateSubscriber(m_subscriber,
                                                       m_domainId,
                                                       m_participantQos,
                                                       m_subscriberQos);
    }

    bool topicValid = !m_topic.is_nil();
    if ((topicValid == false) && (subscriberValid == true))
    {
        topicValid = CreateTopic(m_topicName, m_topicQos);
    }

    bool readerValid = !m_reader.is_nil();
    if ((readerValid == false) && (topicValid == true))
    {
        readerValid = CreateReader(m_readerQos);
    }

    if ((readerValid == true) && (topicValid == true))
    {
        retVal = true;
        ConfigureReadConditions();
    }
    else
    {
        if (readerValid == false)
        {
            std::string error("Failed to create reader.");
            DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
            throw CDdsException(m_topicName, error);
        }

        if (topicValid == false)
        {
            std::string error("Failed to create topic.");
            DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
            throw CDdsException(m_topicName, error);
        }
    }

    if ((m_pWaitsetThread != nullptr) && (retVal == true))
    {
        if (m_pWaitsetThread->Running() == false)
        {
            m_pWaitsetThread->Start();
        }
    }

    m_pFactory->RegisterSignalHandler();

    return retVal;
}

bool CFooReader::Destroy()
{
    LockGuard lock(m_mutexDelegate);
    LockGuard lockMember(m_mutexMember);
    LockGuard lockReader(m_mutexReader);
    LockGuard lockSubscriber(m_mutexSubscriber);
    LockGuard lockTopic(m_mutexTopic);

    while (m_functorVector.empty() == false)
    {
        auto it = m_functorVector.begin();
        CFunctor *pFunctor = *it;
        if (m_pWaitsetThread != nullptr)
        {
            m_pWaitsetThread->UnregisterCondition(pFunctor->GetCondition());
            delete pFunctor;
            m_functorVector.erase(it);
        }
    }

    if (m_pWaitsetThread != nullptr)
    {
        if (m_pWaitsetThread->Stop() == true)
        {
            delete m_pWaitsetThread;
            m_pWaitsetThread = nullptr;
        }
    }

    m_dataDelegateVector.clear();
    m_dataNotAliveVector.clear();
    m_deadlineVector.clear();
    m_livelinessVector.clear();
    m_queryMap.clear();
    m_sampleRejectedVector.clear();
    m_subscriptionVector.clear();

    m_newDataCondition.close();
    m_aliveDataCondition.close();
    m_notAliveCondition.close();

    if (m_reader.is_nil() == false)
    {
        m_reader.close();
    }

    if (m_topic.is_nil() == false)
    {
        m_topic.close();
    }

    return true;
}

CFooReader::CFooReader() :
    m_domainId(DEFAULT_DOMAIN_ID),
    m_topicName(DEFAULT_TOPIC_NAME),
    m_qosProfile(DEFAULT_QOS_PROFILE),
    m_participantQos(DEFAULT_PARTICIPANT_QOS),
    m_subscriberQos(DEFAULT_PUBSUB_QOS),
    m_topicQos(DEFAULT_TOPIC_QOS),
    m_readerQos(DEFAULT_ENTITY_QOS),
    m_pFactory(nullptr),
    m_pWaitsetThread(nullptr),
    m_topic(dds::core::null),
    m_reader(dds::core::null),
    m_subscriber(dds::core::null),
    m_participant(dds::core::null),
    m_newDataCondition(dds::core::null),
    m_aliveDataCondition(dds::core::null),
    m_notAliveCondition(dds::core::null),
    m_deadlineCondition(dds::core::null),
    m_livelinessCondition(dds::core::null),
    m_sampleRejectedCondition(dds::core::null),
    m_subscriptionCondition(dds::core::null)
{

}

CFooReader::CFooReader(uint32_t domainId) :
    m_domainId(domainId),
    m_topicName(DEFAULT_TOPIC_NAME),
    m_qosProfile(DEFAULT_QOS_PROFILE),
    m_participantQos(DEFAULT_PARTICIPANT_QOS),
    m_subscriberQos(DEFAULT_PUBSUB_QOS),
    m_topicQos(DEFAULT_TOPIC_QOS),
    m_readerQos(DEFAULT_ENTITY_QOS),
    m_pFactory(nullptr),
    m_pWaitsetThread(nullptr),
    m_topic(dds::core::null),
    m_reader(dds::core::null),
    m_subscriber(dds::core::null),
    m_participant(dds::core::null),
    m_newDataCondition(dds::core::null),
    m_aliveDataCondition(dds::core::null),
    m_notAliveCondition(dds::core::null),
    m_deadlineCondition(dds::core::null),
    m_livelinessCondition(dds::core::null),
    m_sampleRejectedCondition(dds::core::null),
    m_subscriptionCondition(dds::core::null)
{

}

CFooReader::CFooReader(const std::string topicName) :
    m_domainId(DEFAULT_DOMAIN_ID),
    m_topicName(topicName),
    m_qosProfile(DEFAULT_QOS_PROFILE),
    m_participantQos(DEFAULT_PARTICIPANT_QOS),
    m_subscriberQos(DEFAULT_PUBSUB_QOS),
    m_topicQos(DEFAULT_TOPIC_QOS),
    m_readerQos(DEFAULT_ENTITY_QOS),
    m_pFactory(nullptr),
    m_pWaitsetThread(nullptr),
    m_topic(dds::core::null),
    m_reader(dds::core::null),
    m_subscriber(dds::core::null),
    m_participant(dds::core::null),
    m_newDataCondition(dds::core::null),
    m_aliveDataCondition(dds::core::null),
    m_notAliveCondition(dds::core::null),
    m_deadlineCondition(dds::core::null),
    m_livelinessCondition(dds::core::null),
    m_sampleRejectedCondition(dds::core::null),
    m_subscriptionCondition(dds::core::null)
{

}

CFooReader::CFooReader(uint32_t domainId,
                       const std::string topicName) :
    m_domainId(domainId),
    m_topicName(topicName),
    m_qosProfile(DEFAULT_QOS_PROFILE),
    m_participantQos(DEFAULT_PARTICIPANT_QOS),
    m_subscriberQos(DEFAULT_PUBSUB_QOS),
    m_topicQos(DEFAULT_TOPIC_QOS),
    m_readerQos(DEFAULT_ENTITY_QOS),
    m_pFactory(nullptr),
    m_pWaitsetThread(nullptr),
    m_topic(dds::core::null),
    m_reader(dds::core::null),
    m_subscriber(dds::core::null),
    m_participant(dds::core::null),
    m_newDataCondition(dds::core::null),
    m_aliveDataCondition(dds::core::null),
    m_notAliveCondition(dds::core::null),
    m_deadlineCondition(dds::core::null),
    m_livelinessCondition(dds::core::null),
    m_sampleRejectedCondition(dds::core::null),
    m_subscriptionCondition(dds::core::null)
{

}

CFooReader::CFooReader(const uint32_t domainId,
                       const std::string &topicName,
                       const std::string &qosProfile,
                       const std::string &participantQos,
                       const std::string &subscriberQos,
                       const std::string &topicQos,
                       const std::string &readerQos) :
    m_domainId(domainId),
    m_topicName(topicName),
    m_qosProfile(qosProfile),
    m_participantQos(participantQos),
    m_subscriberQos(subscriberQos),
    m_topicQos(topicQos),
    m_readerQos(readerQos),
    m_pFactory(nullptr),
    m_pWaitsetThread(nullptr),
    m_topic(dds::core::null),
    m_reader(dds::core::null),
    m_subscriber(dds::core::null),
    m_participant(dds::core::null),
    m_newDataCondition(dds::core::null),
    m_aliveDataCondition(dds::core::null),
    m_notAliveCondition(dds::core::null),
    m_deadlineCondition(dds::core::null),
    m_livelinessCondition(dds::core::null),
    m_sampleRejectedCondition(dds::core::null),
    m_subscriptionCondition(dds::core::null)
{

}

CFooReader::~CFooReader()
{
    Destroy();
}

void CFooReader::ReadConditionUpdate(CFunctor * const pFunctor)
{
    LockGuard lock(m_mutexDelegate);

    dds::sub::cond::ReadCondition condition(dds::core::null);
    if (pFunctor->GetCondition(condition) == false)
    {
        std::string error("Failed to update condition on Read update");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        return;
    }

    if (condition == m_newDataCondition)
    {
        ProcessReadCondition(condition, m_dataDelegateVector);
    }
    else if (condition == m_notAliveCondition)
    {
        ProcessReadCondition(condition, m_dataNotAliveVector, true);
    }
}

void CFooReader::StatusConditionUpdate(CFunctor * const pFunctor)
{
    LockGuard lock(m_mutexDelegate);

    if (pFunctor->ConditionEqual(m_deadlineCondition) == true)
    {
        ProcessDeadline();
    }
    else if (pFunctor->ConditionEqual(m_livelinessCondition) == true)
    {
        ProcessLiveliness();
    }
    else if (pFunctor->ConditionEqual(m_sampleRejectedCondition) == true)
    {
        ProcessSampleRejected();
    }
    else if (pFunctor->ConditionEqual(m_subscriptionCondition) == true)
    {
        ProcessSubscriptionMatched();
    }
}

void CFooReader::ProcessReadCondition(dds::sub::cond::ReadCondition &condition,
                                      DataDelegateVector &vector,
                                      bool take)
{
    SampleSeq samples = ReadWithCondition(condition, take);
    if (samples.size() > 0)
    {
        for (auto sampleIt = samples.begin(); sampleIt < samples.end(); ++sampleIt)
        {
            for (auto it = vector.begin(); it != vector.end(); it++)
            {
                DataDelegate delegate = *it;
                Sample sample(*sampleIt);
                delegate.Execute(sample);
            }
        }
    }
}

void CFooReader::ProcessQueryCondition(dds::sub::cond::QueryCondition &condition,
                                       std::string &expression)
{
    SampleSeq samples = QueryWithCondition(condition);
    if (samples.size() > 0)
    {
        LockGuard lock(m_mutexDelegate);
        auto mapIt = m_queryMap.find(expression);
        if (mapIt != m_queryMap.end())
        {
            QueryVector *pVector = &mapIt->second;
            for (auto it = pVector->begin(); it != pVector->end(); it++)
            {
                DataSeqDelegate delegate = it->second;
                delegate.Execute(samples);
            }
        }
    }
}

void CFooReader::ProcessDeadline()
{
    try
    {
        dds::core::status::RequestedDeadlineMissedStatus status = m_reader.requested_deadline_missed_status();
        for (auto it = m_deadlineVector.begin(); it != m_deadlineVector.end(); it++)
        {
            it->Execute(status);
        }
    }
    catch (const dds::core::Exception& e)
    {
        std::string error("Failed to get requested deadline missed status ["+std::string(e.what())+"]");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }
}

void CFooReader::ProcessLiveliness()
{
    try
    {
        dds::core::status::LivelinessChangedStatus status = m_reader.liveliness_changed_status();
        for (auto it = m_livelinessVector.begin(); it != m_livelinessVector.end(); it++)
        {
            it->Execute(status);
        }
    }
    catch (const dds::core::Exception& e)
    {
        std::string error("Failed to get liveliness changed status ["+std::string(e.what())+"]");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }
}

void CFooReader::ProcessSampleRejected()
{
    try
        {
            dds::core::status::SampleRejectedStatus status = m_reader.sample_rejected_status();
            for (auto it = m_sampleRejectedVector.begin(); it != m_sampleRejectedVector.end(); it++)
            {
                it->Execute(status);
            }
        }
        catch (const dds::core::Exception& e)
        {
            std::string error("Failed to get sample rejected status ["+std::string(e.what())+"]");
            DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
            throw CDdsException(m_topicName, error);
        }
}

void CFooReader::ProcessSubscriptionMatched()
{
    try
    {
        dds::core::status::SubscriptionMatchedStatus status = m_reader.subscription_matched_status();
        for (auto it = m_subscriptionVector.begin(); it != m_subscriptionVector.end(); it++)
        {
            it->Execute(status);
        }
    }
    catch (const dds::core::Exception& e)
    {
        std::string error("Failed to get subscription matched status ["+std::string(e.what())+"]");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }
}

CFooReader::Sample CFooReader::ReadWithCondition(dds::sub::cond::ReadCondition &condition,
                                                 ReadBuffer direction,
                                                 bool take,
                                                 dds::core::InstanceHandle handle)
{
    Sample sample;
    LockGuard lock(m_mutexReader);

    if (m_reader.is_nil() == false)
    {
        typename dds::sub::DataReaderFoo::Selector selector(m_reader);
        selector.state(condition.state_filter());

        if (direction == READ_BEGIN)
        {
            selector.max_samples(1);
        }

        if (handle.is_nil() == false)
        {
            selector.instance(handle);
        }

        try
        {
            if (take == false)
            {
                 dds::sub::LoanedSamplesFoo samples(selector.read());
                 if (samples.length() > 0)
                 {
                     if (direction == READ_BEGIN)
                     {
                         sample = std::move(samples[0]);
                     }
                     else if (direction == READ_END)
                     {
                         sample = std::move(samples[samples.length()-1]);
                     }
                 }
            }
            else
            {
                dds::sub::LoanedSamplesFoo samples(selector.take());
                if (samples.length() > 0)
                {
                    if (direction == READ_BEGIN)
                    {
                        sample = std::move(samples[0]);
                    }
                    else if (direction == READ_END)
                    {
                        sample = std::move(samples[samples.length()-1]);
                    }
                }
            }

        }
        catch (const dds::core::Exception& e)
        {
            std::string error("Failed to read ["+std::string(e.what())+"]");
            DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
            throw CDdsException(m_topicName, error);
        }
    }

    return sample;
}

CFooReader::SampleSeq CFooReader::ReadWithCondition(dds::sub::cond::ReadCondition &condition,
                                                    bool take,
                                                    dds::core::InstanceHandle handle)
{
    SampleSeq vector;

    LockGuard lock(m_mutexReader);

    if (m_reader.is_nil() == false)
    {
        typename dds::sub::DataReaderFoo::Selector selector(m_reader);
        selector.state(condition.state_filter());
        if (handle.is_nil() == false)
        {
            selector.instance(handle);
        }

        try
        {
            if (take == false)
            {
                dds::sub::LoanedSamplesFoo samples(selector.read());
                std::transform(rti::sub::valid_samples(samples.begin()),
                               rti::sub::valid_samples(samples.end()),
                               std::back_inserter(vector),
                               rti::sub::copy_to_sampleFoo);
            }
            else
            {
                dds::sub::LoanedSamplesFoo samples(selector.take());
                std::transform(rti::sub::valid_samples(samples.begin()),
                               rti::sub::valid_samples(samples.end()),
                               std::back_inserter(vector),
                               rti::sub::copy_to_sampleFoo);
            }
        }
        catch (const dds::core::Exception& e)
        {
            std::string error("Failed to read ["+std::string(e.what())+"]");
            DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
            throw CDdsException(m_topicName, error);
        }
    }

    return vector;
}

CFooReader::SampleSeq CFooReader::QueryWithCondition(dds::sub::cond::QueryCondition &condition,
                                                     bool take,
                                                     dds::core::InstanceHandle handle)
{
    SampleSeq vector;

    LockGuard lock(m_mutexReader);

    if (m_reader.is_nil() == false)
    {
        dds::sub::Query query(m_reader, condition.expression(), condition.parameters());

        typename dds::sub::DataReaderFoo::Selector selector(m_reader);
        selector.state(condition.state_filter());
        selector.content(query);
        if (handle.is_nil() == false)
        {
            selector.instance(handle);
        }

        try
        {
            if (take == false)
            {
                dds::sub::LoanedSamplesFoo samples(selector.read());
                std::transform(rti::sub::valid_samples(samples.begin()),
                               rti::sub::valid_samples(samples.end()),
                               std::back_inserter(vector),
                               rti::sub::copy_to_sampleFoo);
            }
            else
            {
                dds::sub::LoanedSamplesFoo samples(selector.take());
                std::transform(rti::sub::valid_samples(samples.begin()),
                               rti::sub::valid_samples(samples.end()),
                               std::back_inserter(vector),
                               rti::sub::copy_to_sampleFoo);
            }
        }
        catch (const dds::core::Exception& e)
        {
            std::string error("Failed to read query ["+std::string(e.what())+"]");
            DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
            throw CDdsException(m_topicName, error);
        }
    }
    else
    {
        std::string error("Cannot execute query: reader is null.");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }

    return vector;
}

bool CFooReader::CreateTopic(const std::string topicName,
                             const std::string &topicQos)
{
    bool retVal = false;

    LockGuard lock(m_mutexTopic);

    if (m_participant.is_nil() == true)
    {
        std::string error("Cannot create topic - participant is null");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        return false;
    }

    bool topicNull = m_topic.is_nil();
    if ((topicNull == true) && (m_pFactory != nullptr))
    {
        dds::topic::qos::TopicQos qos;
        bool updateQos = m_pFactory->GetTopicQos(qos, topicQos);

        if (updateQos == true)
        {
            try
            {
                m_topic = dds::topic::find<Topic>(m_participant, topicName);
                if (m_topic.is_nil() == true)
                {
                    m_topic = dds::topic::TopicFoo(m_participant,
                                                     topicName,
                                                     qos,
                                                     nullptr,
                                                     dds::core::status::StatusMask::none());
                }

                retVal = !m_topic.is_nil();
            }
            catch (const dds::core::Exception& e)
            {
                retVal = false;
                std::string error("Failed to create topic["+std::string(e.what())+"]");
                DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
                throw CDdsException(m_topicName, error);
            }
        }
    }

    return retVal;
}

bool CFooReader::DeleteTopic()
{
    bool retVal = false;

    LockGuard lock(m_mutexTopic);

    if (m_topic.is_nil() == false)
    {
        m_topic.close();
        retVal = true;
    }

    return retVal;
}

bool CFooReader::CreateReader(const std::string &qosProfile)
{
    bool retVal = false;

    LockGuard lock(m_mutexReader);

    bool readerNull = m_reader.is_nil();
    if ((readerNull == true) && (m_pFactory != nullptr))
    {
        dds::sub::qos::DataReaderQos qos;
        bool updateQos = m_pFactory->GetReaderQos(qos, qosProfile);
        bool subNull = m_subscriber.is_nil();
        bool topicNull = m_topic.is_nil();
        if ((updateQos == true) && (subNull == false) && (topicNull == false))
        {
            try
            {
                m_reader = dds::sub::DataReaderFoo(m_subscriber,
                                                 m_topic,
                                                 qos,
                                                 nullptr,
                                                 dds::core::status::StatusMask::none());
                retVal = true;
            }
            catch (const dds::core::Exception& e)
            {
                retVal = false;
                std::string error("DDS: Failed to create reader ["+std::string(e.what())+"]");
                DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
                throw CDdsException(m_topicName, error);
            }
        }
    }

    return retVal;
}

bool CFooReader::DeleteReader()
{
    bool retVal = false;

    LockGuard lock(m_mutexReader);

    if (m_reader.is_nil() == false)
    {
        m_reader.close();
        retVal = true;
    }

    return retVal;
}

bool CFooReader::CreateReadCondition(dds::sub::cond::ReadCondition &condition,
                                     const dds::sub::status::SampleState sampleState,
                                     const dds::sub::status::ViewState viewState,
                                     const dds::sub::status::InstanceState instanceState,
                                     CFunctor::Callback callback,
                                     bool registerWaitset)
{
    bool retVal = false;

    if (m_reader.is_nil() == false)
    {
        if ((callback != nullptr) && (registerWaitset == true))
        {
            CFunctor *pFunctor = new CFunctor(callback);
            dds::sub::status::DataState state(sampleState, viewState, instanceState);
            condition = dds::sub::cond::ReadCondition(m_reader, state, *pFunctor);
            pFunctor->SetCondition(condition);
            m_functorVector.push_back(pFunctor);
            if (m_pWaitsetThread->RegisterCondition(pFunctor) == false)
            {
                std::string error("Failed to register read condition with waitset thread.");
                DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
                throw CDdsException(m_topicName, error);
            }
        }
        else
        {
            dds::sub::status::DataState state(sampleState, viewState, instanceState);
            condition = dds::sub::cond::ReadCondition(m_reader, state);
        }
    }
    else
    {
        std::string error("Failed to create condition: reader is null.");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }

    return retVal;
}

void CFooReader::ConfigureReadConditions()
{
    CFunctor::Callback callback =
            std::bind(&CFooReader::ReadConditionUpdate,
                      this,
                      std::placeholders::_1);

    if (m_newDataCondition.is_nil() == true)
    {
        CreateReadCondition(m_newDataCondition,
                            dds::sub::status::SampleState::not_read(),
                            dds::sub::status::ViewState::any(),
                            dds::sub::status::InstanceState::alive(),
                            callback,
                            true);
    }

    if (m_notAliveCondition.is_nil() == true)
    {
        CreateReadCondition(m_notAliveCondition,
                            dds::sub::status::SampleState::any(),
                            dds::sub::status::ViewState::any(),
                            dds::sub::status::InstanceState::not_alive_disposed(),
                            callback,
                            true);
    }

    if (m_aliveDataCondition.is_nil() == true)
    {
        CreateReadCondition(m_aliveDataCondition,
                            dds::sub::status::SampleState::any(),
                            dds::sub::status::ViewState::any(),
                            dds::sub::status::InstanceState::alive());
    }
}

void CFooReader::EnableConditionDeadline()
{
    ENABLE_STATUS_CONDITION_READER(m_deadlineCondition,
                                   dds::core::status::StatusMask::requested_deadline_missed(),
                                   CFooReader);
}

void CFooReader::EnableConditionLiveliness()
{
    ENABLE_STATUS_CONDITION_READER(m_livelinessCondition,
                                   dds::core::status::StatusMask::liveliness_changed(),
                                   CFooReader);
}

void CFooReader::EnableConditionSampleRejected()
{
    ENABLE_STATUS_CONDITION_READER(m_sampleRejectedCondition,
                                   dds::core::status::StatusMask::sample_rejected(),
                                   CFooReader);
}

void CFooReader::EnableConditionSubscriptionMatched()
{
    ENABLE_STATUS_CONDITION_READER(m_subscriptionCondition,
                                   dds::core::status::StatusMask::subscription_matched(),
                                   CFooReader);
}

void CFooReader::DisableConditionDeadline()
{
    LockGuard lock(m_mutexMember);
    m_pWaitsetThread->UnregisterCondition(m_deadlineCondition);
    DeleteFunctor(m_deadlineCondition);
}

void CFooReader::DisableConditionLiveliness()
{
    LockGuard lock(m_mutexMember);
    m_pWaitsetThread->UnregisterCondition(m_livelinessCondition);
    DeleteFunctor(m_livelinessCondition);
}

void CFooReader::DisableConditionSampleRejected()
{
    LockGuard lock(m_mutexMember);
    m_pWaitsetThread->UnregisterCondition(m_sampleRejectedCondition);
    DeleteFunctor(m_sampleRejectedCondition);
}

void CFooReader::DisableConditionSubscriptionMatched()
{
    LockGuard lock(m_mutexMember);
    m_pWaitsetThread->UnregisterCondition(m_subscriptionCondition);
    DeleteFunctor(m_subscriptionCondition);
}


CFooReader::DataDelegateIt CFooReader::FindDataDelegate(common::OwnerWeakPtr pOwner)
{
    DataDelegateIt it = m_dataDelegateVector.end();
    for (auto findIt = m_dataDelegateVector.begin(); findIt != m_dataDelegateVector.end(); findIt++)
    {
        DataDelegate delegate = *findIt;
        if (delegate.Equal(pOwner))
        {
            it = findIt;
            break;
        }
    }

    return it;
}

CFooReader::DataDelegateIt CFooReader::FindNotAliveDelegate(common::OwnerWeakPtr pOwner)
{
    DataDelegateIt it = m_dataNotAliveVector.end();
    for (auto findIt = m_dataNotAliveVector.begin(); findIt != m_dataNotAliveVector.end(); findIt++)
    {
        DataDelegate delegate = *findIt;
        if (delegate.Equal(pOwner))
        {
            it = findIt;
            break;
        }
    }
    return it;
}

CFooReader::QueryVectorIt CFooReader::FindQueryExpression(QueryVector &vector,
                                                          common::OwnerWeakPtr pOwner)
{
    QueryVectorIt it = vector.end();
    for (auto findIt = vector.begin(); findIt != vector.end(); findIt++)
    {
        QueryPair *pPair = &*findIt;
        DataSeqDelegate *pDelegate = &pPair->second;
        if (pDelegate->Equal(pOwner))
        {
            it = findIt;
            break;
        }
    }

    return it;
}

CFooReader::FunctorIt CFooReader::FindFunctor(CFunctor *pFunctor)
{
    for (CFooReader::FunctorIt it = m_functorVector.begin(); it != m_functorVector.end(); it++)
    {
        if (pFunctor == *it)
        {
            return it;
        }
    }

    return m_functorVector.end();
}

CFooReader::FunctorIt CFooReader::FindFunctor(dds::core::cond::Condition &condition)
{
    for (CFooReader::FunctorIt it = m_functorVector.begin(); it != m_functorVector.end(); it++)
    {
        CFunctor *pFunctor = *it;
        if (pFunctor->ConditionEqual(condition) == true)
        {
            return it;
        }
    }

    return m_functorVector.end();
}

CFooReader::FunctorIt CFooReader::FindFunctor(dds::core::cond::StatusCondition &condition)
{
    for (CFooReader::FunctorIt it = m_functorVector.begin(); it != m_functorVector.end(); it++)
    {
        CFunctor *pFunctor = *it;
        if (pFunctor->ConditionEqual(condition) == true)
        {
            return it;
        }
    }

    return m_functorVector.end();
}

CFooReader::FunctorIt CFooReader::FindFunctor(dds::sub::cond::QueryCondition &condition)
{
    for (CFooReader::FunctorIt it = m_functorVector.begin(); it != m_functorVector.end(); it++)
    {
        CFunctor *pFunctor = *it;
        if (pFunctor->ConditionEqual(condition) == true)
        {
            return it;
        }
    }

    return m_functorVector.end();
}

CFooReader::FunctorIt CFooReader::FindFunctor(dds::sub::cond::ReadCondition &condition)
{
    for (CFooReader::FunctorIt it = m_functorVector.begin(); it != m_functorVector.end(); it++)
    {
        CFunctor *pFunctor = *it;
        if (pFunctor->ConditionEqual(condition) == true)
        {
            return it;
        }
    }

    return m_functorVector.end();
}

bool CFooReader::DeleteFunctor(CFunctor *pFunctor)
{
    bool retVal = false;

    if (pFunctor == nullptr)
    {
        std::string error("Failed to delete NULL Functor.");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        return false;
    }

    FunctorIt it = FindFunctor(pFunctor);
    if (it != m_functorVector.end())
    {
        m_functorVector.erase(it);
        delete pFunctor;
    }

    return retVal;
}

bool CFooReader::DeleteFunctor(dds::core::cond::Condition &condition)
{
    bool retVal = false;

    FunctorIt it = FindFunctor(condition);
    if (it != m_functorVector.end())
    {
        CFunctor *pFunctor = *it;
        delete pFunctor;
        m_functorVector.erase(it);
    }

    return retVal;
}

bool CFooReader::DeleteFunctor(dds::core::cond::StatusCondition &condition)
{
    bool retVal = false;

    FunctorIt it = FindFunctor(condition);
    if (it != m_functorVector.end())
    {
        CFunctor *pFunctor = *it;
        delete pFunctor;
        m_functorVector.erase(it);
    }

    return retVal;
}

bool CFooReader::DeleteFunctor(dds::sub::cond::QueryCondition &condition)
{
    bool retVal = false;

    FunctorIt it = FindFunctor(condition);
    if (it != m_functorVector.end())
    {
        CFunctor *pFunctor = *it;
        delete pFunctor;
        m_functorVector.erase(it);
    }

    return retVal;
}

bool CFooReader::DeleteFunctor(dds::sub::cond::ReadCondition &condition)
{
    bool retVal = false;

    FunctorIt it = FindFunctor(condition);
    if (it != m_functorVector.end())
    {
        CFunctor *pFunctor = *it;
        delete pFunctor;
        m_functorVector.erase(it);
    }

    return retVal;
}
