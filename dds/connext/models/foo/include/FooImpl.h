

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from FooImpl.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef FooImpl_982569390_h
#define FooImpl_982569390_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_c_h
#include "ndds/ndds_c.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

extern const char *Foo_cTYPENAME;

typedef struct Foo_c {

    DDS_Long   x ;
    DDS_Long   y ;

    Foo_c() {}
} Foo_c ;
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Foo_c_get_typecode(void); /* Type code */

DDS_SEQUENCE(Foo_cSeq, Foo_c);                                        

NDDSUSERDllExport
RTIBool Foo_c_initialize(
    Foo_c* self);

NDDSUSERDllExport
RTIBool Foo_c_initialize_ex(
    Foo_c* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Foo_c_initialize_w_params(
    Foo_c* self,
    const struct DDS_TypeAllocationParams_t * allocParams);        

NDDSUSERDllExport
void Foo_c_finalize(
    Foo_c* self);

NDDSUSERDllExport
void Foo_c_finalize_ex(
    Foo_c* self,RTIBool deletePointers);

NDDSUSERDllExport
void Foo_c_finalize_w_params(
    Foo_c* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Foo_c_finalize_optional_members(
    Foo_c* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Foo_c_copy(
    Foo_c* dst,
    const Foo_c* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* FooImpl */

