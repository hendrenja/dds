

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Foo.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef Foo_982569390_hpp
#define Foo_982569390_hpp

#include <iosfwd>
#include "FooImpl.h"

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport __declspec(dllexport)
#endif

#include "dds/domain/DomainParticipant.hpp"
#include "dds/topic/TopicTraits.hpp"
#include "dds/core/SafeEnumeration.hpp"
#include "dds/core/String.hpp"
#include "dds/core/array.hpp"
#include "dds/core/vector.hpp"
#include "dds/core/Optional.hpp"
#include "dds/core/xtypes/DynamicType.hpp"
#include "dds/core/xtypes/StructType.hpp"
#include "dds/core/xtypes/UnionType.hpp"
#include "dds/core/xtypes/EnumType.hpp"
#include "dds/core/xtypes/AliasType.hpp"
#include "rti/core/array.hpp"
#include "rti/util/StreamFlagSaver.hpp"
#include "rti/domain/PluginSupport.hpp"
#include "rti/core/LongDouble.hpp"
#include "rti/core/Pointer.hpp"
#include "rti/topic/TopicTraits.hpp"
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

class NDDSUSERDllExport Foo {

  public:
    Foo();
    Foo(
        int32_t x,
        int32_t y);

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifndef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    Foo (Foo&& other_) = default;
    Foo& operator=(Foo&&  other_) = default;
    Foo& operator=(const Foo&) = default;
    Foo(const Foo&) = default;
    #else
    Foo(Foo&& other_) OMG_NOEXCEPT;  
    Foo& operator=(Foo&&  other_) OMG_NOEXCEPT;
    #endif
    #endif 

    int32_t x() const OMG_NOEXCEPT;
    void x(int32_t value);

    int32_t y() const OMG_NOEXCEPT;
    void y(int32_t value);

    bool operator == (const Foo& other_) const;
    bool operator != (const Foo& other_) const;

    void swap(Foo& other_) OMG_NOEXCEPT ;

  private:

    int32_t m_x_;
    int32_t m_y_;

};

inline void swap(Foo& a, Foo& b)  OMG_NOEXCEPT 
{
    a.swap(b);
}

NDDSUSERDllExport std::ostream& operator << (std::ostream& o,const Foo& sample);

namespace dds { 
    namespace topic {

        template<>
        struct topic_type_name<Foo> {
            NDDSUSERDllExport static std::string value() {
                return "Foo";
            }
        };

        template<>
        struct is_topic_type<Foo> : public dds::core::true_type {};

        template<>
        struct topic_type_support<Foo> {

            NDDSUSERDllExport static void initialize_sample(Foo& sample);

            NDDSUSERDllExport static void register_type(
                dds::domain::DomainParticipant& participant,
                const std::string & type_name);

            NDDSUSERDllExport static std::vector<char>& to_cdr_buffer(
                std::vector<char>& buffer, const Foo& sample);

            NDDSUSERDllExport static void from_cdr_buffer(Foo& sample, const std::vector<char>& buffer);
        };

    }
}

namespace rti { 
    namespace topic {
        template<>
        struct dynamic_type<Foo> {
            typedef dds::core::xtypes::StructType type;
            NDDSUSERDllExport static const dds::core::xtypes::StructType& get();
        };

        template<>
        struct impl_type<Foo> {
            typedef Foo_c type;
        };

    }
}

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif // Foo_982569390_hpp

