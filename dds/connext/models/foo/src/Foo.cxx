

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Foo.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include <iosfwd>
#include <iomanip>
#include "Foo.hpp"
#include "FooImplPlugin.h"

// ---- Foo: 

Foo::Foo() :
    m_x_ (0) ,
    m_y_ (0) {
}   

Foo::Foo (
    int32_t x,
    int32_t y)
    :
        m_x_( x ),
        m_y_( y ) {
}

#ifdef RTI_CXX11_RVALUE_REFERENCES
#ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
Foo::Foo(Foo&& other_) OMG_NOEXCEPT  :m_x_ (std::move(other_.m_x_))
,
m_y_ (std::move(other_.m_y_))
{
} 

Foo& Foo::operator=(Foo&&  other_) OMG_NOEXCEPT {
    Foo tmp(std::move(other_));
    swap(tmp); 
    return *this;
}
#endif
#endif   

void Foo::swap(Foo& other_)  OMG_NOEXCEPT 
{
    using std::swap;
    swap(m_x_, other_.m_x_);
    swap(m_y_, other_.m_y_);
}  

bool Foo::operator == (const Foo& other_) const {
    if (m_x_ != other_.m_x_) {
        return false;
    }
    if (m_y_ != other_.m_y_) {
        return false;
    }
    return true;
}
bool Foo::operator != (const Foo& other_) const {
    return !this->operator ==(other_);
}

// --- Getters and Setters: -------------------------------------------------
int32_t Foo::x() const OMG_NOEXCEPT{
    return m_x_;
}

void Foo::x(int32_t value) {
    m_x_ = value;
}

int32_t Foo::y() const OMG_NOEXCEPT{
    return m_y_;
}

void Foo::y(int32_t value) {
    m_y_ = value;
}

std::ostream& operator << (std::ostream& o,const Foo& sample){
    rti::util::StreamFlagSaver flag_saver (o);
    o <<"[";
    o << "x: " << sample.x()<<", ";
    o << "y: " << sample.y() ;
    o <<"]";
    return o;
}

// --- Type traits: -------------------------------------------------

namespace rti { 
    namespace topic {

        const dds::core::xtypes::StructType& dynamic_type<Foo>::get()
        {
            return static_cast<const dds::core::xtypes::StructType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(Foo_c_get_typecode())));
        }

    }
}  

namespace dds { 
    namespace topic {
        void topic_type_support<Foo>:: register_type(
            dds::domain::DomainParticipant& participant,
            const std::string& type_name){

            rti::domain::register_type_plugin(
                participant,
                type_name,
                Foo_cPlugin_new,
                Foo_cPlugin_delete);
        }

        void topic_type_support<Foo>::initialize_sample(Foo& sample){

            Foo_c* native_sample=reinterpret_cast<Foo_c*> (&sample);

            struct DDS_TypeDeallocationParams_t deAllocParams = {RTI_FALSE, RTI_FALSE};
            Foo_c_finalize_w_params(native_sample,&deAllocParams);

            struct DDS_TypeAllocationParams_t allocParams = {RTI_FALSE, RTI_FALSE, RTI_TRUE}; 
            RTIBool ok=Foo_c_initialize_w_params(native_sample,&allocParams);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to initialize_w_params");

        } 

        std::vector<char>& topic_type_support<Foo>::to_cdr_buffer(
            std::vector<char>& buffer, const Foo& sample)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = Foo_cPlugin_serialize_to_cdr_buffer(
                NULL, &length,reinterpret_cast<const Foo_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = Foo_cPlugin_serialize_to_cdr_buffer(
                &buffer[0], &length, reinterpret_cast<const Foo_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;

        }

        void topic_type_support<Foo>::from_cdr_buffer(Foo& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = Foo_cPlugin_deserialize_from_cdr_buffer(
                reinterpret_cast<Foo_c*> (&sample), &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to create Foo from cdr buffer");
        }

    }
}  

