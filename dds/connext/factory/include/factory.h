#ifndef __OASYS_CORE_DDS_CONNEXT_FACTORY_FACTORY_H__
#define __OASYS_CORE_DDS_CONNEXT_FACTORY_FACTORY_H__

#include <mutex>
#include <stdio.h>
#include <oasys/core/common/common.h>
#include <oasys/core/dds/connext/factory/ndds/hpp/dds/dds.hpp>

namespace oasys
{
namespace core
{

#define DDS_FACTORY_ERROR_STRINGIFY(x) #x
#define DDS_FACTORY_ERROR_TO_STRING(x) DDS_FACTORY_ERROR_STRINGIFY(x)
#define DDS_FACTORY_ERROR_AT __FILE__ ":" DDS_FACTORY_ERROR_TO_STRING(__LINE__)
#define DDS_FACTORY_ERROR(error) DdsFactory::GetInstance()->Error(DDS_FACTORY_ERROR_AT, error)
#define DDS_FACTORY_TOPIC_ERROR(topic, error) DdsFactory::GetInstance()->Error(DDS_FACTORY_ERROR_AT, topic, error)

class CDdsFactory
{
public:
    typedef oasys::core::common::TDataDelegate<const std::string &> ErrorDelegate;
    typedef std::function<void()> SignalHandlerNotification;

    bool CreateDomainParticipant(dds::domain::DomainParticipant &participant,
                                 int32_t domainId,
                                 const std::string &qosProfile);
    bool CreatePublisher(dds::pub::Publisher &publisher,
                         int32_t domainId,
                         const std::string &participantQos,
                         const std::string &publisherQos);
    bool DeletePublisher(int32_t domainId,
                         const std::string &qosProfile);
    bool CreateSubscriber(dds::sub::Subscriber &subscriber,
                          int32_t domainId,
                          const std::string &participantQos,
                          const std::string &subscriberQos);
    bool DeleteSubscriber(int32_t domainId,
                          const std::string &qosProfile);
    bool GetWriterQos(dds::pub::qos::DataWriterQos &qos,
                      const std::string &writerQos);
    bool GetReaderQos(dds::sub::qos::DataReaderQos &qos,
                      const std::string &readerQos);
    bool GetTopicQos(dds::topic::qos::TopicQos &qos,
                      const std::string &topicQos);

    void SetSignalHandler(const SignalHandlerNotification &callback);
    bool Initialize(const std::string &qosFileUri);
    bool Initialize(const std::string &qosFileUri,
                    const std::string &defaultProfile);
    bool Destroy();

    void RegisterSignalHandler();
    bool RegisterErrorDelegate(ErrorDelegate &delegate);
    bool UnregisterErrorDelegate(oasys::core::common::OwnerSharedPtr pOwner);
    void Error(const char* location, const std::string &message);
    void Error(const char* location, const std::string &topic, const std::string &message);

    CDdsFactory();
    virtual ~CDdsFactory();

private:
    typedef std::map<std::string, dds::domain::DomainParticipant> DomainProfileMap;
    typedef std::pair<std::string, dds::domain::DomainParticipant> DomainProfileMapPair;
    typedef std::map<int32_t, DomainProfileMap> DomainMap;
    typedef std::map<std::string, dds::pub::Publisher> PublisherProfileMap;
    typedef std::pair<std::string, dds::pub::Publisher> PublisherProfileMapPair;
    typedef std::map<int32_t, PublisherProfileMap> PublisherMap;
    typedef std::map<std::string, dds::sub::Subscriber> SubscriberProfileMap;
    typedef std::pair<std::string, dds::sub::Subscriber> SubscriberProfileMapPair;
    typedef std::map<int32_t, SubscriberProfileMap> SubscriberMap;
    typedef std::vector<ErrorDelegate> ErrorVector;

    bool VerifyLicenseEnv();
    void ProcessErrorHandlers(const std::string &message);

    dds::core::QosProvider      *m_pQosProvider;
    SignalHandlerNotification   m_signalNotifier;

    DomainMap                   m_domainMap;
    PublisherMap                m_publisherMap;
    SubscriberMap               m_subscriberMap;
    ErrorVector                 m_errorVector;

    std::mutex                  m_mutex;
    std::mutex                  m_delegateMutex;
};

template <typename Vector> bool FindDelegate(Vector vector,
                                             common::OwnerWeakPtr pOwner)
{
    bool found = false;

    for (auto findIt = vector.begin(); findIt != vector.end(); findIt++)
    {
        auto delegate = *findIt;
        if (delegate.Equal(pOwner))
        {
            found = true;
            break;
        }
    }

    return found;
}

template <typename Vector, typename Delegate> bool RegisterSubscriber(Vector &vector,
                                                                      Delegate &delegate)
{
    bool retVal = false;

    if (FindDelegate<Vector>(vector, delegate.GetOwnerPtr()) == false)
    {
        return false;
    }

    retVal = FindDelegate<Vector>(vector, delegate.GetOwnerPtr());

    return retVal;
}

template <typename Vector> bool UnregisterSubscriber(Vector &vector,
                                                     common::OwnerSharedPtr pOwner)
{
    bool retVal = false;

    auto it = vector.end();
    for (it = vector.begin(); it != vector.end(); it++)
    {
        auto delegate = *it;
        if (delegate.Equal(pOwner))
        {
            retVal = true;
            break;
        }
    }

    if ((retVal == true) && (it != vector.end()))
    {
        vector.erase(it);
    }

    return retVal;
}

typedef common::TSingleton<CDdsFactory> DdsFactory;

}
}
#endif //__OASYS_CORE_DDS_CONNEXT_FACTORY_FACTORY_H__
