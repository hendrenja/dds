/* _type.h
 *
 * Type definitions for C-language.
 * This file contains generated code. Do not modify!
 */

#ifndef FACTORY__TYPE_H
#define FACTORY__TYPE_H

#include <corto/core/_type.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Casting macro's */

/* Native types */
#ifndef FACTORY_H
#endif

/* Type definitions */
#ifdef __cplusplus
}
#endif
#endif

