#ifndef __OASYS_CORE_DDS_CONNEXT_FACTORY_FOO_READER_H__
#define __OASYS_CORE_DDS_CONNEXT_FACTORY_FOO_READER_H__

#include <oasys/core/dds/connext/factory/factory.h>
#include <oasys/core/dds/connext/factory/waitset_thread.h>
#include <oasys/core/common/common.h>
#include <oasys/core/dds/connext/models/foo/Foo.hpp>

namespace oasys
{
namespace core
{
namespace interfaces
{
namespace foo
{

typedef enum _ReadBuffer
{
    READ_BEGIN = 0,
    READ_END = 1
} ReadBuffer;


class CFooReader
{
public:
    /**
     * \defgroup Type Definitions
     * @{
     */
    ///
    /// \brief Public type definitions for DDS Reader
    ///
    typedef dds::sub::Sample<Foo> Sample;
    typedef std::vector<Sample> SampleSeq;
    typedef std::function<void(Sample &sample)> DataNotifyCallback;
    typedef std::function<void(SampleSeq &sampleSeq)> DataSeqNotifyCallback;
    typedef oasys::core::common::TDataDelegate<Sample> DataDelegate;
    typedef oasys::core::common::TDataDelegate<SampleSeq> DataSeqDelegate;
    typedef oasys::core::common::TDataDelegate<dds::core::status::RequestedDeadlineMissedStatus> DeadlineDelegate;
    typedef oasys::core::common::TDataDelegate<dds::core::status::LivelinessChangedStatus> LivelinessDelegate;
    typedef oasys::core::common::TDataDelegate<dds::core::status::SampleRejectedStatus> SampleRejectedDelegate;
    typedef oasys::core::common::TDataDelegate<dds::core::status::SubscriptionMatchedStatus> SubscriptionMatchedDelegate;
    typedef std::vector<std::string> ParamVector;
    /**@}*/

    /**
     * \defgroup Data Accessors
     * @{
     */
    ///
    /// \brief Methods for getting data from DDS middleware
    ///
    virtual SampleSeq Query(std::string &expression);
    virtual SampleSeq Query(std::string &expression,
                            ParamVector &params);
    virtual Sample Read(ReadBuffer direction = READ_END);
    virtual SampleSeq ReadSeq();
    virtual Sample Take(ReadBuffer direction = READ_END,
                        dds::core::InstanceHandle handle = dds::core::InstanceHandle::nil());
    virtual SampleSeq TakeSeq(dds::core::InstanceHandle handle = dds::core::InstanceHandle::nil());
    /**@}*/

    /**
     * \defgroup Register Async Data Handlers
     * @{
     */
    ///
    /// \brief Register delegates for receiving DDS data samples asynchronously
    ///
    bool RegisterNewDataSubscriber(DataDelegate &delegate);
    bool UnregisterNewDataSubscriber(oasys::core::common::OwnerSharedPtr pOwner);
    bool RegisterInstanceNotAliveSubscriber(DataDelegate &delegate);
    bool UnregisterInstanceNotAliveSubscriber(oasys::core::common::OwnerSharedPtr pOwner);
    virtual bool RegisterQuery(std::string expression,
                               ParamVector &params,
                               DataSeqDelegate &delegate);
    bool UnRegisterQuery(std::string &expression,
                         oasys::core::common::OwnerSharedPtr pOwner);
    /**@}*/

    /**
     * \defgroup Register Async Interface Status Handlers
     * @{
     */
    ///
    /// \brief Register delegates for receiving DDS interface status updates
    ///
    bool RegisterDeadlineSubscriber(DeadlineDelegate &delegate);
    bool UnregisterDeadlineSubscriber(oasys::core::common::OwnerSharedPtr pOwner);
    bool RegisterLivelinessSubscriber(LivelinessDelegate &delegate);
    bool UnregisterLivelinessSubscriber(oasys::core::common::OwnerSharedPtr pOwner);
    bool RegisterSampleRejectedSubscriber(SampleRejectedDelegate &delegate);
    bool UnregisterSampleRejectedSubscriber(oasys::core::common::OwnerSharedPtr pOwner);
    bool RegisterSubscriptionMatchedSubscriber(SubscriptionMatchedDelegate &delegate);
    bool UnregisterSubscriptionMatchedSubscriber(oasys::core::common::OwnerSharedPtr pOwner);
    /**@}*/

    /**
     * \defgroup Configuration
     * @{
     */
    ///
    /// \brief Configure DDS Reader
    ///
    void SetTopicName(std::string name);
    void SetQosProfile(std::string qos);
    void SetParticipantQos(std::string qos);
    void SetSubscriberQos(std::string qos);
    void SetTopicQos(std::string qos);
    void SetReaderQos(std::string qos);

    virtual bool Initialize();
    bool Destroy();

    CFooReader();
    CFooReader(uint32_t domainId);
    CFooReader(const std::string topicName);
    CFooReader(uint32_t domainId,
               const std::string topicName);
    CFooReader(const uint32_t domainId,
               const std::string &topicName,
               const std::string &qosProfile,
               const std::string &participantQos,
               const std::string &subscriberQos,
               const std::string &topicQos,
               const std::string &readerQos);
    virtual ~CFooReader();
    /**@}*/

protected:
    typedef std::vector<DataDelegate> DataDelegateVector;

    virtual void ReadConditionUpdate(CFunctor * const pFunctor);
    virtual void StatusConditionUpdate(CFunctor * const pFunctor);

    virtual void ProcessReadCondition(dds::sub::cond::ReadCondition &condition,
                                      DataDelegateVector &vector,
                                      bool take = false);
    virtual void ProcessQueryCondition(dds::sub::cond::QueryCondition &condition,
                                       std::string &expression);
    virtual void ProcessDeadline();
    virtual void ProcessLiveliness();
    virtual void ProcessSampleRejected();
    virtual void ProcessSubscriptionMatched();

    Sample ReadWithCondition(dds::sub::cond::ReadCondition &condition,
                             ReadBuffer direction = READ_END,
                             bool take = false,
                             dds::core::InstanceHandle handle = dds::core::null);
    SampleSeq ReadWithCondition(dds::sub::cond::ReadCondition &condition,
                                bool take = false,
                                dds::core::InstanceHandle handle = dds::core::null);
    SampleSeq QueryWithCondition(dds::sub::cond::QueryCondition &condition,
                                 bool take = false,
                                 dds::core::InstanceHandle handle = dds::core::null);

    bool CreateTopic(const std::string topicName,
                     const std::string &topicQos);
    bool DeleteTopic();
    bool CreateReader(const std::string &qosProfile);
    bool DeleteReader();
    bool CreateReadCondition(dds::sub::cond::ReadCondition &condition,
                             const dds::sub::status::SampleState sampleState,
                             const dds::sub::status::ViewState viewState,
                             const dds::sub::status::InstanceState instanceState,
                             CFunctor::Callback callback = nullptr,
                             bool registerWaitset = false);
    void ConfigureReadConditions();

private:
    typedef std::function<void(dds::sub::cond::ReadCondition)> ReadNotifyCallback;
    typedef std::function<void(dds::core::cond::StatusCondition)> StatusNotifyCallback;
    typedef std::pair<dds::sub::cond::QueryCondition, DataSeqDelegate> QueryPair;
    typedef std::vector<QueryPair> QueryVector;
    typedef std::map<std::string, QueryVector> QueryMap;
    typedef std::pair<std::string, QueryVector> QueryMapPair;
    typedef std::vector<DeadlineDelegate> DeadlineVector;
    typedef std::vector<LivelinessDelegate> LivelinessVector;
    typedef std::vector<SampleRejectedDelegate> SampleRejectedVector;
    typedef std::vector<SubscriptionMatchedDelegate> SubscriptionVector;
    typedef std::vector<dds::sub::cond::ReadCondition> ReadConditionVector;
    typedef std::vector<CFunctor*> FunctorVector;
    typedef FunctorVector::iterator FunctorIt;
    typedef DataDelegateVector::iterator DataDelegateIt;
    typedef QueryVector::iterator QueryVectorIt;
    typedef dds::topic::Topic<Foo> Topic;
    typedef dds::sub::DataReader<Foo> Reader;

    virtual void EnableConditionDeadline();
    virtual void EnableConditionLiveliness();
    virtual void EnableConditionSampleRejected();
    virtual void EnableConditionSubscriptionMatched();
    void DisableConditionDeadline();
    void DisableConditionLiveliness();
    void DisableConditionSampleRejected();
    void DisableConditionSubscriptionMatched();

    DataDelegateIt FindDataDelegate(oasys::core::common::OwnerWeakPtr pOwner);
    DataDelegateIt FindNotAliveDelegate(oasys::core::common::OwnerWeakPtr pOwner);
    QueryVectorIt FindQueryExpression(QueryVector &vector,
                                      oasys::core::common::OwnerWeakPtr pOwner);
    FunctorIt FindFunctor(CFunctor *pFunctor);
    FunctorIt FindFunctor(dds::core::cond::Condition &condition);
    FunctorIt FindFunctor(dds::core::cond::StatusCondition &condition);
    FunctorIt FindFunctor(dds::sub::cond::QueryCondition &condition);
    FunctorIt FindFunctor(dds::sub::cond::ReadCondition &condition);
    bool DeleteFunctor(CFunctor *pFunctor);
    bool DeleteFunctor(dds::core::cond::Condition &condition);
    bool DeleteFunctor(dds::core::cond::StatusCondition &condition);
    bool DeleteFunctor(dds::sub::cond::QueryCondition &condition);
    bool DeleteFunctor(dds::sub::cond::ReadCondition &condition);

    uint32_t                            m_domainId;
    std::string                         m_topicName;
    std::string                         m_qosProfile;
    std::string                         m_participantQos;
    std::string                         m_subscriberQos;
    std::string                         m_topicQos;
    std::string                         m_readerQos;

    CDdsFactory                         *m_pFactory;
    CWaitsetThread                      *m_pWaitsetThread;
    Topic                               m_topic;
    Reader                              m_reader;
    dds::sub::Subscriber                m_subscriber;
    dds::domain::DomainParticipant      m_participant;

    dds::sub::cond::ReadCondition       m_newDataCondition;
    dds::sub::cond::ReadCondition       m_aliveDataCondition;
    dds::sub::cond::ReadCondition       m_notAliveCondition;
    dds::core::cond::StatusCondition    m_deadlineCondition;
    dds::core::cond::StatusCondition    m_livelinessCondition;
    dds::core::cond::StatusCondition    m_sampleRejectedCondition;
    dds::core::cond::StatusCondition    m_subscriptionCondition;

    DataDelegateVector                  m_dataDelegateVector;
    DataDelegateVector                  m_dataNotAliveVector;
    DeadlineVector                      m_deadlineVector;
    LivelinessVector                    m_livelinessVector;
    QueryMap                            m_queryMap;
    SampleRejectedVector                m_sampleRejectedVector;
    SubscriptionVector                  m_subscriptionVector;
    FunctorVector                       m_functorVector;

    std::mutex                          m_mutexDelegate;
    std::mutex                          m_mutexReader;
    std::mutex                          m_mutexSubscriber;
    std::mutex                          m_mutexTopic;
    std::mutex                          m_mutexMember;
};



}
}
}
}
#endif //__OASYS_CORE_DDS_CONNEXT_FACTORY_FOO_READER_H__
