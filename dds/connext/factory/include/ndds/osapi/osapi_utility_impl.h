/* (c) Copyright 2003-2015, Real-Time Innovations, Inc. All rights reserved. */
/*
 * @(#)osapi_utility_impl.h    generated by: makeheader    Mon Apr 25 07:49:32 2016
 *
 *		built from:	utility_impl.ifc
 */

#ifndef osapi_utility_impl_h
#define osapi_utility_impl_h


#ifdef __cplusplus
    extern "C" {
#endif


#define RTIOsapiUtility_max(l, r) (((l) > (r)) ? (l) : (r))
#define RTIOsapiUtility_min(l, r) (((l) < (r)) ? (l) : (r))
#define RTIOsapiUtility_isWithinBound(n, lower, upper) (((n) >= (lower)) \
                                                        && ((n) <= (upper)))

#define RTIOsapiUtility_unusedParameter(parameter) (void) parameter


#ifdef __cplusplus
    }	/* extern "C" */
#endif

#endif /* osapi_utility_impl_h */
