/* $Id$

 (c) Copyright, Real-Time Innovations, 2015-2016.
 All rights reserved.

 No duplications, whole or partial, manual or electronic, may be made
 without express written permission.  Any such copies, or
 revisions thereof, must display this notice unaltered.
 This code contains trade secrets of Real-Time Innovations, Inc.

modification history
--------------------

===================================================================== */

#ifndef RTI_CONFIG_BUILDID_H
#define RTI_CONFIG_BUILDID_H

#ifndef RTI_BUILD_NUMBER_STRING
  #define RTI_BUILD_NUMBER_STRING "NDDSCPP2_VERSION_5.2.3.0_BUILD_2016-04-25T07:37:01-07:00_RTI_RELEASE"
#endif

#endif /* RTI_CONFIG_BUILDID_H */
