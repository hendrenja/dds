#ifndef __OASYS_CORE_DDS_CONNEXT_FACTORY_WAITSET_THREAD_H__
#define __OASYS_CORE_DDS_CONNEXT_FACTORY_WAITSET_THREAD_H__

#include <oasys/core/common/common.h>
#include <oasys/core/dds/connext/factory/functor.h>
#include <oasys/core/dds/connext/factory/ndds/hpp/dds/dds.hpp>
#include <mutex>
#include <thread>

namespace oasys
{
namespace core
{

class CWaitsetThread
{
public:
    bool RegisterCondition(CFunctor * const pFunctor);
    bool UnregisterCondition(const dds::core::cond::Condition &condition);
    void Timeout(dds::core::Duration duration);
    dds::core::Duration Timeout();

    bool Running();
    void Start();
    bool Stop();
    void Destroy();

    CWaitsetThread(const std::string topicName);
    virtual ~CWaitsetThread();

private:
    typedef std::vector<CFunctor*> FunctorVector;
    typedef FunctorVector::iterator FunctorIt;

    virtual void Run();
    void SetRunning(bool running);
    bool SetupWaitset();
    void DestroyWaitSet();
    FunctorIt FindFunctor(const dds::core::cond::Condition &condition);
    FunctorIt FindFunctor(CFunctor * const pFunctor);

    FunctorVector                   m_functorVector;

    dds::core::cond::WaitSet        m_waitSet;
    dds::core::Duration             m_timeout;
    dds::core::cond::GuardCondition m_stopCondition;
    dds::core::cond::GuardCondition m_updateCondition;

    bool                            m_running;
    std::string                     m_topicName;

    std::thread                     m_thread;
    std::mutex                      m_mutex;
    std::mutex                      m_threadMutex;
    std::mutex                      m_mutexMembers;


};

}
}

#endif //__OASYS_CORE_DDS_CONNEXT_FACTORY_WAITSET_THREAD_H__
