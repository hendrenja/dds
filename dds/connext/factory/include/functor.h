#ifndef __OASYS_CORE_DDS_CONNEXT_FACTORY_FUNCTORS_H__
#define __OASYS_CORE_DDS_CONNEXT_FACTORY_FUNCTORS_H__

#include <string>
#include <signal.h>
#include <oasys/core/dds/connext/factory/ndds/hpp/dds/dds.hpp>

namespace oasys
{
namespace core
{

class CFunctor
{
public:
    typedef std::function<void(CFunctor * const)> Callback;

    std::string GetExpression();
    void SetExpression(const std::string &expression);
    dds::core::cond::Condition GetCondition(void);

    /// Query Condition Handlers
    bool GetCondition(dds::sub::cond::QueryCondition &condition);
    void SetCondition(const dds::sub::cond::QueryCondition &condition);

    /// Read Condition Handlers
    bool GetCondition(dds::sub::cond::ReadCondition &condition);
    void SetCondition(const dds::sub::cond::ReadCondition &condition);

    /// Status Condition Handlers
    bool GetCondition(dds::core::cond::StatusCondition &condition);
    void SetCondition(const dds::core::cond::StatusCondition &condition);

    bool ConditionEqual(const dds::core::cond::Condition &condition);

    void operator() ();
    CFunctor(const Callback &callback,
             std::string expression);
    CFunctor(const Callback &callback);
    virtual ~CFunctor();

private:
    CFunctor                            *m_pThis;
    Callback                            m_callback;
    std::string                         m_expression;
    dds::sub::cond::QueryCondition      m_queryCondition;
    dds::sub::cond::ReadCondition       m_readCondition;
    dds::core::cond::StatusCondition    m_statusCondition;
};

}
}

#endif // __OASYS_CORE_DDS_CONNEXT_FACTORY_FUNCTORS_H__
