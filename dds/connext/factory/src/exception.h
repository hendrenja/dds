#ifndef __OASYS_CORE_DDS_CONNEXT_FACTORY_EXCEPTION_H__
#define __OASYS_CORE_DDS_CONNEXT_FACTORY_EXCEPTION_H__

#include <string>
#include <oasys/core/common/exception_list.h>
#include <oasys/core/dds/connext/factory/factory.h>

namespace oasys
{
namespace core
{

class CDdsException : public OASYS_DDS_CONNEXT_EXCEPTION
{
public:
    explicit CDdsException(const std::string topic,
                           const std::string message)
    {
        std::string error("DDS Exception Topic ["+topic+"] Message["+message+"]");
        DDS_FACTORY_ERROR(error);
        m_message.assign(error.c_str());
    }
};

}
}

#endif // __OASYS_CORE_DDS_CONNEXT_FACTORY_EXCEPTION_H__
