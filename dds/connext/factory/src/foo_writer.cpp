#include <oasys/core/dds/connext/factory/foo_writer.h>
#include <oasys/core/dds/connext/factory/functor.h>
#include <exception.h>
#include <factory_macros.h>

const std::string DEFAULT_QOS_URI = "/usr/local/etc/corto/1.1/oasys/core/dds/connext/factory/qos.xml";

const uint32_t      DEFAULT_DOMAIN_ID       = 0;
const std::string   DEFAULT_TOPIC_NAME      = "Foo";
const std::string   DEFAULT_QOS_PROFILE     = "OASYS::BestEffort";
const std::string   DEFAULT_PARTICIPANT_QOS = "OASYS::BestEffort";
const std::string   DEFAULT_PUBSUB_QOS      = "OASYS::BestEffort";
const std::string   DEFAULT_TOPIC_QOS       = "OASYS::BestEffort";
const std::string   DEFAULT_ENTITY_QOS      = "OASYS::BestEffort";


using namespace oasys::core;
using namespace oasys::core::interfaces::foo;

typedef std::lock_guard<std::mutex> LockGuard;

dds::core::InstanceHandle CFooWriter::RegisterInstance(Foo &data)
{
    dds::core::InstanceHandle handle = dds::core::InstanceHandle::nil();

    LockGuard lock(m_mutexWriter);

    if (m_writer.is_nil() == false)
    {
        try
        {
            handle = m_writer.register_instance(data);
        }
        catch (const dds::core::Exception& e)
        {
            handle = dds::core::InstanceHandle::nil();

            std::string error("Failed to register instance handle ["+std::string(e.what())+"]");
            DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
            throw CDdsException(m_topicName, error);
        }
    }

    return handle;
}

bool CFooWriter::UnregisterInstance(dds::core::InstanceHandle &handle, bool dispose)
{
    bool retVal = false;

    LockGuard lock(m_mutexWriter);

    if (m_writer.is_nil() == false)
    {
        try
        {
            if (dispose == true)
            {
                m_writer.dispose_instance(handle);
            }
            else
            {
                m_writer.unregister_instance(handle);
            }
            retVal = true;
        }
        catch (const dds::core::Exception& e)
        {
            retVal = false;
            std::string error("Failed to unregister instance ["+std::string(e.what())+"]");
            DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
            throw CDdsException(m_topicName, error);
        }
    }

    return retVal;
}

bool CFooWriter::Write(Foo &data)
{
    bool retVal = false;

    LockGuard lock(m_mutexWriter);

    if (m_writer.is_nil() == false)
    {
        try
        {
            m_writer.write(data);
            retVal = true;
        }
        catch (const dds::core::Exception& e)
        {
            std::string error("Failed to write ["+std::string(e.what())+"]");
            DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
            throw CDdsException(m_topicName, error);
            retVal = false;
        }
    }

    return retVal;
}

bool CFooWriter::Write(Foo &data, dds::core::InstanceHandle &handle)
{
    bool retVal = false;

    LockGuard lock(m_mutexWriter);

    if (m_writer.is_nil() == false)
    {
        try
        {
            m_writer.write(data, handle);
            retVal = true;
        }
        catch (const dds::core::Exception& e)
        {
            retVal = false;
            std::string error("Failed to write ["+std::string(e.what())+"]");
            DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
            throw CDdsException(m_topicName, error);
        }
    }

    return retVal;
}

bool CFooWriter::RegisterDeadlineSubscriber(DeadlineDelegate &delegate)
{
    LockGuard lock(m_mutexDelegate);
    return RegisterSubscriber<DeadlineVector, DeadlineDelegate>(m_deadlineVector, delegate);
}

bool CFooWriter::UnregisterDeadlineSubscriber(oasys::core::common::OwnerSharedPtr pOwner)
{
    LockGuard lock(m_mutexDelegate);
    return UnregisterSubscriber<DeadlineVector>(m_deadlineVector, pOwner);
}

bool CFooWriter::RegisterLivelinessSubscriber(LivelinessDelegate &delegate)
{
    LockGuard lock(m_mutexDelegate);
    return RegisterSubscriber<LivelinessVector, LivelinessDelegate>(m_livelinessVector, delegate);
}

bool CFooWriter::UnregisterLivelinessSubscriber(oasys::core::common::OwnerSharedPtr pOwner)
{
    LockGuard lock(m_mutexDelegate);
    return UnregisterSubscriber<LivelinessVector>(m_livelinessVector, pOwner);
}

bool CFooWriter::RegisterPublicationMatchedSubscriber(PublicationMatchedDelegate &delegate)
{
    LockGuard lock(m_mutexDelegate);
    return RegisterSubscriber<PublicationVector, PublicationMatchedDelegate>(m_publicationVector, delegate);
}

bool CFooWriter::UnregisterPublicationMatchedSubscriber(oasys::core::common::OwnerSharedPtr pOwner)
{
    LockGuard lock(m_mutexDelegate);
    return UnregisterSubscriber<PublicationVector>(m_publicationVector, pOwner);
}

void CFooWriter::SetTopicName(const std::string &name)
{
    LockGuard lock(m_mutexDelegate);
    m_topicName = name;
}

void CFooWriter::SetQosProfile(const std::string &qos)
{
    LockGuard lock(m_mutexDelegate);
    m_qosProfile = qos;
}

void CFooWriter::SetParticipantQos(const std::string &qos)
{
    LockGuard lock(m_mutexDelegate);
    m_participantQos = qos;
}

void CFooWriter::SetPublisherQos(const std::string &qos)
{
    LockGuard lock(m_mutexDelegate);
    m_publisherQos = qos;
}

void CFooWriter::SetTopicQos(const std::string &qos)
{
    LockGuard lock(m_mutexDelegate);
    m_topicQos = qos;
}

void CFooWriter::SetWriterQos(const std::string &qos)
{
    LockGuard lock(m_mutexDelegate);
    m_writerQos = qos;
}

bool CFooWriter::Initialize()
{
    bool retVal = false;

    LockGuard lock(m_mutexMember);

    if (m_pFactory == nullptr)
    {
        m_pFactory = DdsFactory::GetInstance();
        if (m_pFactory != nullptr)
        {
            ///TODO Get URI FROM ENV
            if (m_pFactory->Initialize(DEFAULT_QOS_URI) == false)
            {
                std::string error("Failed to initialize factory with [" + DEFAULT_QOS_URI + "] xml.");
                DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
                return false;
            }
        }
    }

    if (m_pWaitsetThread == nullptr)
    {
        m_pWaitsetThread = new CWaitsetThread(m_topicName);
    }

    bool participantValid = !m_participant.is_nil();
    if (participantValid == false)
    {
        participantValid = m_pFactory->CreateDomainParticipant(m_participant,
                                                               m_domainId,
                                                               m_participantQos);
    }

    bool publisherValid = !m_publisher.is_nil();
    if ((publisherValid == false) && (participantValid == true))
    {
        publisherValid = m_pFactory->CreatePublisher(m_publisher,
                                                     m_domainId,
                                                     m_participantQos,
                                                     m_publisherQos);
    }

    bool topicValid = !m_topic.is_nil();
    if ((topicValid == false) && (publisherValid == true))
    {
        topicValid = CreateTopic(m_topicName, m_topicQos);
    }

    bool writerValid = !m_writer.is_nil();
    if ((writerValid == false) && (topicValid == true))
    {
        writerValid = CreateWriter(m_writerQos);
    }

    if ((writerValid == true) && (topicValid == true))
    {
        retVal = true;
    }
    else
    {
        if (writerValid == false)
        {
            std::string error("Failed to create writer.");
            DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
            throw CDdsException(m_topicName, error);
        }

        if (topicValid == false)
        {
            std::string error("Failed to create topic.");
            DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
            throw CDdsException(m_topicName, error);
        }
    }

    if ((m_pWaitsetThread != nullptr) && (retVal == true))
    {
        if (m_pWaitsetThread->Running() == false)
        {
            m_pWaitsetThread->Start();
        }
    }

    m_pFactory->RegisterSignalHandler();

    return retVal;
}

bool CFooWriter::Destroy()
{
    LockGuard lock(m_mutexDelegate);
    LockGuard lockMember(m_mutexMember);
    LockGuard lockWriter(m_mutexWriter);
    LockGuard lockPublisher(m_mutexPublisher);
    LockGuard lockTopic(m_mutexTopic);

    while (m_functorVector.empty() == false)
    {
        auto it = m_functorVector.begin();
        CFunctor *pFunctor = *it;
        if (m_pWaitsetThread != nullptr)
        {
            m_pWaitsetThread->UnregisterCondition(pFunctor->GetCondition());
            delete pFunctor;
            m_functorVector.erase(it);
        }
    }

    m_deadlineVector.clear();
    m_livelinessVector.clear();
    m_publicationVector.clear();

    if (m_pWaitsetThread != nullptr)
    {
        if (m_pWaitsetThread->Stop() == true)
        {
            delete m_pWaitsetThread;
            m_pWaitsetThread = nullptr;
        }
    }

    if (m_writer.is_nil() == false)
    {
        m_writer.close();
    }

    if (m_topic.is_nil() == false)
    {
        m_topic.close();
    }

    return true;
}

CFooWriter::CFooWriter() :
    m_domainId(DEFAULT_DOMAIN_ID),
    m_topicName(DEFAULT_TOPIC_NAME),
    m_qosProfile(DEFAULT_QOS_PROFILE),
    m_participantQos(DEFAULT_PARTICIPANT_QOS),
    m_publisherQos(DEFAULT_PUBSUB_QOS),
    m_topicQos(DEFAULT_TOPIC_QOS),
    m_writerQos(DEFAULT_ENTITY_QOS),
    m_pFactory(nullptr),
    m_pWaitsetThread(nullptr),
    m_topic(dds::core::null),
    m_writer(dds::core::null),
    m_publisher(dds::core::null),
    m_participant(dds::core::null),
    m_deadlineCondition(dds::core::null),
    m_livelinessCondition(dds::core::null),
    m_publicationCondition(dds::core::null)
{

}

CFooWriter::CFooWriter(uint32_t domainId) :
    m_domainId(domainId),
    m_topicName(DEFAULT_TOPIC_NAME),
    m_qosProfile(DEFAULT_QOS_PROFILE),
    m_participantQos(DEFAULT_PARTICIPANT_QOS),
    m_publisherQos(DEFAULT_PUBSUB_QOS),
    m_topicQos(DEFAULT_TOPIC_QOS),
    m_writerQos(DEFAULT_ENTITY_QOS),
    m_pFactory(nullptr),
    m_pWaitsetThread(nullptr),
    m_topic(dds::core::null),
    m_writer(dds::core::null),
    m_publisher(dds::core::null),
    m_participant(dds::core::null),
    m_deadlineCondition(dds::core::null),
    m_livelinessCondition(dds::core::null),
    m_publicationCondition(dds::core::null)
{

}

CFooWriter::CFooWriter(const std::string topicName) :
    m_domainId(DEFAULT_DOMAIN_ID),
    m_topicName(topicName),
    m_qosProfile(DEFAULT_QOS_PROFILE),
    m_participantQos(DEFAULT_PARTICIPANT_QOS),
    m_publisherQos(DEFAULT_PUBSUB_QOS),
    m_topicQos(DEFAULT_TOPIC_QOS),
    m_writerQos(DEFAULT_ENTITY_QOS),
    m_pFactory(nullptr),
    m_pWaitsetThread(nullptr),
    m_topic(dds::core::null),
    m_writer(dds::core::null),
    m_publisher(dds::core::null),
    m_participant(dds::core::null),
    m_deadlineCondition(dds::core::null),
    m_livelinessCondition(dds::core::null),
    m_publicationCondition(dds::core::null)
{

}

CFooWriter::CFooWriter(uint32_t domainId,
                       const std::string topicName) :
    m_domainId(domainId),
    m_topicName(topicName),
    m_qosProfile(DEFAULT_QOS_PROFILE),
    m_participantQos(DEFAULT_PARTICIPANT_QOS),
    m_publisherQos(DEFAULT_PUBSUB_QOS),
    m_topicQos(DEFAULT_TOPIC_QOS),
    m_writerQos(DEFAULT_ENTITY_QOS),
    m_pFactory(nullptr),
    m_pWaitsetThread(nullptr),
    m_topic(dds::core::null),
    m_writer(dds::core::null),
    m_publisher(dds::core::null),
    m_participant(dds::core::null),
    m_deadlineCondition(dds::core::null),
    m_livelinessCondition(dds::core::null),
    m_publicationCondition(dds::core::null)
{

}

CFooWriter::CFooWriter(const uint32_t domainId,
                       const std::string topicName,
                       const std::string &qosProfile,
                       const std::string &participantQos,
                       const std::string &publisherQos,
                       const std::string &topicQos,
                       const std::string &writerQos) :
    m_domainId(domainId),
    m_topicName(topicName),
    m_qosProfile(qosProfile),
    m_participantQos(participantQos),
    m_publisherQos(publisherQos),
    m_topicQos(topicQos),
    m_writerQos(writerQos),
    m_pFactory(nullptr),
    m_pWaitsetThread(nullptr),
    m_topic(dds::core::null),
    m_writer(dds::core::null),
    m_publisher(dds::core::null),
    m_participant(dds::core::null),
    m_deadlineCondition(dds::core::null),
    m_livelinessCondition(dds::core::null),
    m_publicationCondition(dds::core::null)
{

}

CFooWriter::~CFooWriter()
{
    Destroy();
}

void CFooWriter::StatusConditionUpdate(CFunctor * const pFunctor)
{
    LockGuard lock(m_mutexDelegate);

    if (pFunctor->ConditionEqual(m_deadlineCondition) == true)
    {
        ProcessDeadline();
    }
    else if (pFunctor->ConditionEqual(m_livelinessCondition) == true)
    {
        ProcessLiveliness();
    }
    else if (pFunctor->ConditionEqual(m_publicationCondition) == true)
    {
        ProcessPublicationMatched();
    }
}

void CFooWriter::ProcessDeadline()
{
    try
    {
        dds::core::status::OfferedDeadlineMissedStatus status = m_writer.offered_deadline_missed_status();
        for (auto it = m_deadlineVector.begin(); it != m_deadlineVector.end(); it++)
        {
            it->Execute(status);
        }
    }
    catch (const dds::core::Exception& e)
    {
        std::string error("Failed to get offered deadline missed status ["+std::string(e.what())+"]");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }
}

void CFooWriter::ProcessLiveliness()
{
    try
    {
        dds::core::status::LivelinessLostStatus status = m_writer.liveliness_lost_status();
        for (auto it = m_livelinessVector.begin(); it != m_livelinessVector.end(); it++)
        {
            it->Execute(status);
        }
    }
    catch (const dds::core::Exception& e)
    {
        std::string error("Failed to get liveliness lost status ["+std::string(e.what())+"]");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }
}

void CFooWriter::ProcessPublicationMatched()
{
    try
    {
        dds::core::status::PublicationMatchedStatus status = m_writer.publication_matched_status();
        for (auto it = m_publicationVector.begin(); it != m_publicationVector.end(); it++)
        {
            it->Execute(status);
        }
    }
    catch (const dds::core::Exception& e)
    {
        std::string error("Failed to get publication matched status ["+std::string(e.what())+"]");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }
}

bool CFooWriter::CreateTopic(const std::string topicName,
                             const std::string &topicQos)
{
    bool retVal = false;

    LockGuard lock(m_mutexTopic);

    if (m_participant.is_nil() == true)
    {
        std::string error("Cannot create Topic - Participant is null");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
        return false;
    }

    bool topicNull = m_topic.is_nil();
    if ((topicNull == true) && (m_pFactory != nullptr))
    {
        dds::topic::qos::TopicQos qos;
        bool updateQos = m_pFactory->GetTopicQos(qos, topicQos);

        if (updateQos == true)
        {
            try
            {
                m_topic = dds::topic::find<Topic>(m_participant, topicName);
                if (m_topic.is_nil() == true)
                {
                    m_topic = dds::topic::Topic<Foo>(m_participant,
                                                     topicName,
                                                     qos,
                                                     nullptr,
                                                     dds::core::status::StatusMask::none());
                }

                retVal = !m_topic.is_nil();
            }
            catch (const dds::core::Exception& e)
            {
                retVal = false;
                std::string error("Failed to create topic ["+std::string(e.what())+"]");
                DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
                throw CDdsException(m_topicName, error);
            }
        }
    }

    return retVal;
}

bool CFooWriter::DeleteTopic()
{
    bool retVal = false;

    LockGuard lock(m_mutexTopic);

    if (m_topic.is_nil() == false)
    {
        m_topic.close();
        retVal = true;
    }

    return retVal;
}

bool CFooWriter::CreateWriter(const std::string &qosProfile)
{
    bool retVal = false;

    LockGuard lock(m_mutexWriter);

    bool writerNull = m_writer.is_nil();
    if ((writerNull == true) && (m_pFactory != nullptr))
    {
        dds::pub::qos::DataWriterQos qos;
        bool updateQos = m_pFactory->GetWriterQos(qos, qosProfile);
        bool subNull = m_publisher.is_nil();
        bool topicNull = m_topic.is_nil();
        if ((updateQos == true) && (subNull == false) && (topicNull == false))
        {
            try
            {
                m_writer = dds::pub::DataWriter<Foo>(m_publisher,
                                                 m_topic,
                                                 qos,
                                                 nullptr,
                                                 dds::core::status::StatusMask::none());
                retVal = true;
            }
            catch (const dds::core::Exception& e)
            {
                retVal = false;
                std::string error("Failed to create writer ["+std::string(e.what())+"]");
                DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
                throw CDdsException(m_topicName, error);
            }
        }
    }

    return retVal;
}

bool CFooWriter::DeleteWriter()
{
    bool retVal = false;

    LockGuard lock(m_mutexWriter);

    if (m_writer.is_nil() == false)
    {
        m_writer.close();
        retVal = true;
    }

    return retVal;
}

void CFooWriter::EnableConditionDeadline()
{
    ENABLE_STATUS_CONDITION_WRITER(m_deadlineCondition,
                                   dds::core::status::StatusMask::requested_deadline_missed(),
                                   CFooWriter);
}

void CFooWriter::EnableConditionLiveliness()
{
    ENABLE_STATUS_CONDITION_WRITER(m_livelinessCondition,
                                   dds::core::status::StatusMask::liveliness_changed(),
                                   CFooWriter);
}

void CFooWriter::EnableConditionPublicationMatched()
{
    ENABLE_STATUS_CONDITION_WRITER(m_publicationCondition,
                                   dds::core::status::StatusMask::publication_matched(),
                                   CFooWriter);
}

void CFooWriter::DisableConditionDeadline()
{
    LockGuard lock(m_mutexMember);
    m_pWaitsetThread->UnregisterCondition(m_deadlineCondition);
    DeleteFunctor(m_deadlineCondition);
}

void CFooWriter::DisableConditionLiveliness()
{
    LockGuard lock(m_mutexMember);
    m_pWaitsetThread->UnregisterCondition(m_livelinessCondition);
    DeleteFunctor(m_livelinessCondition);
}

void CFooWriter::DisableConditionPublicationMatched()
{
    LockGuard lock(m_mutexMember);
    m_pWaitsetThread->UnregisterCondition(m_publicationCondition);
    DeleteFunctor(m_publicationCondition);
}

CFooWriter::FunctorIt CFooWriter::FindFunctor(CFunctor *pFunctor)
{
    for (FunctorIt it = m_functorVector.begin(); it != m_functorVector.end(); it++)
    {
        if (pFunctor == *it)
        {
            return it;
        }
    }

    return m_functorVector.end();
}

CFooWriter::FunctorIt CFooWriter::FindFunctor(dds::core::cond::StatusCondition &condition)
{
    for (FunctorIt it = m_functorVector.begin(); it != m_functorVector.end(); it++)
    {
        CFunctor *pFunctor = *it;
        if (pFunctor->ConditionEqual(condition) == true)
        {
            return it;
        }
    }

    return m_functorVector.end();
}

bool CFooWriter::DeleteFunctor(CFunctor *pFunctor)
{
    bool retVal = false;

    if (pFunctor == nullptr)
    {
        std::string error("Failed to delete null functor");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        return false;
    }

    FunctorIt it = FindFunctor(pFunctor);
    if (it != m_functorVector.end())
    {
        m_functorVector.erase(it);
        delete pFunctor;
    }

    return retVal;
}

bool CFooWriter::DeleteFunctor(dds::core::cond::StatusCondition &condition)
{
    bool retVal = false;

    FunctorIt it = FindFunctor(condition);
    if (it != m_functorVector.end())
    {
        CFunctor *pFunctor = *it;
        delete pFunctor;
        m_functorVector.erase(it);
    }

    return retVal;
}
