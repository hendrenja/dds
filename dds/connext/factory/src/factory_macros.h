#ifndef __OASYS_CORE_DDS_CONNEXT_FACTORY_MACROS_H__
#define __OASYS_CORE_DDS_CONNEXT_FACTORY_MACROS_H__

#define DDS_EXCEPTION_CATCH_BLOCK  \
catch (dds::core::InvalidDowncastError &exception)\
{\
    throw OASYS_DDS_CONNEXT_EXCEPTION(exception.what());\
}\
catch (dds::core::UnsupportedError &exception)\
{\
    throw OASYS_DDS_CONNEXT_EXCEPTION(exception.what());\
}\
catch (dds::core::TimeoutError &exception)\
{\
    throw OASYS_DDS_CONNEXT_EXCEPTION(exception.what());\
}\
catch (dds::core::PreconditionNotMetError &exception)\
{\
    throw OASYS_DDS_CONNEXT_EXCEPTION(exception.what());\
}\
catch (dds::core::OutOfResourcesError &exception)\
{\
    throw OASYS_DDS_CONNEXT_EXCEPTION(exception.what());\
}\
catch (dds::core::NotEnabledError &exception)\
{\
    throw OASYS_DDS_CONNEXT_EXCEPTION(exception.what());\
}\
catch (dds::core::InvalidArgumentError &exception)\
{\
    throw OASYS_DDS_CONNEXT_EXCEPTION(exception.what());\
}\
catch (dds::core::InconsistentPolicyError &exception)\
{\
    throw OASYS_DDS_CONNEXT_EXCEPTION(exception.what());\
}\
catch (dds::core::ImmutablePolicyError &exception)\
{\
    throw OASYS_DDS_CONNEXT_EXCEPTION(exception.what());\
}\
catch (dds::core::IllegalOperationError &exception)\
{\
    throw OASYS_DDS_CONNEXT_EXCEPTION(exception.what());\
}\
catch (dds::core::AlreadyClosedError &exception)\
{\
    throw OASYS_DDS_CONNEXT_EXCEPTION(exception.what());\
}\
catch (dds::core::Error &exception)\
{\
    throw OASYS_DDS_CONNEXT_EXCEPTION(exception.what());\
}

#define ENABLE_STATUS_CONDITION_READER(condition, statusMask, reader)  \
LockGuard lock(m_mutexMember); \
\
if (m_deadlineCondition.is_nil() == true)\
{\
    CFunctor::Callback callback = std::bind(&reader::StatusConditionUpdate,\
                                          this,\
                                          std::placeholders::_1);\
    CFunctor *pFunctor = new CFunctor(callback);\
    printf("Create Functor [%p]\n", pFunctor);\
    if (m_reader.is_nil() == false)\
    {\
        condition = dds::core::cond::StatusCondition(m_reader);\
        dds::core::status::StatusMask mask = statusMask;\
        condition->enabled_statuses(mask);\
        if (pFunctor != nullptr)\
        {\
            pFunctor->SetCondition(condition);\
        }\
        m_functorVector.push_back(pFunctor);\
    }\
    else\
    {\
        throw OASYS_DDS_CONNEXT_EXCEPTION(m_topicName + "Failed to create status condition: reader is null.");\
    }\
\
    if (m_pWaitsetThread->RegisterCondition(pFunctor) == false)\
    {\
        throw OASYS_DDS_CONNEXT_EXCEPTION(m_topicName + "Failed to register status condition functor.");\
    }\
}


#define ENABLE_STATUS_CONDITION_WRITER(condition, statusMask, writer)  \
LockGuard lock(m_mutexMember); \
\
if (m_deadlineCondition.is_nil() == true)\
{\
    CFunctor::Callback callback = std::bind(&writer::StatusConditionUpdate,\
                                          this,\
                                          std::placeholders::_1);\
    CFunctor *pFunctor = new CFunctor(callback);\
    if (m_writer.is_nil() == false)\
    {\
        condition = dds::core::cond::StatusCondition(m_writer);\
        dds::core::status::StatusMask mask = statusMask;\
        condition->enabled_statuses(mask);\
        if (pFunctor != nullptr)\
        {\
            pFunctor->SetCondition(condition);\
        }\
        m_functorVector.push_back(pFunctor);\
    }\
    else\
    {\
        throw OASYS_DDS_CONNEXT_EXCEPTION(m_topicName + "Failed to create status condition: writer is null.");\
    }\
\
    if (m_pWaitsetThread->RegisterCondition(pFunctor) == false)\
    {\
        throw OASYS_DDS_CONNEXT_EXCEPTION(m_topicName + "Failed to register status condition functor.");\
    }\
}

#endif // __OASYS_CORE_DDS_CONNEXT_FACTORY_MACROS_H__
