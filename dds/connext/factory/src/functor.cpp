#include <oasys/core/dds/connext/factory/functor.h>
#include <oasys/core/dds/connext/factory/factory.h>
#include "exception.h"

using namespace oasys::core;

std::string CFunctor::GetExpression()
{
    return m_expression;
}

void CFunctor::SetExpression(const std::string &expression)
{
    m_expression = expression;
}

dds::core::cond::Condition oasys::core::CFunctor::GetCondition()
{
    if (m_queryCondition.is_nil() == false)
    {
        return m_queryCondition;
    }

    if (m_readCondition.is_nil() == false)
    {
        return m_readCondition;
    }

    if (m_statusCondition.is_nil() == false)
    {
        return m_statusCondition;
    }

    return dds::core::null;
}

bool CFunctor::GetCondition(dds::sub::cond::QueryCondition &condition)
{
    bool retVal = false;

    if (m_queryCondition.is_nil() == false)
    {
        condition = m_queryCondition;
        retVal = true;
    }

    return retVal;
}

void CFunctor::SetCondition(const dds::sub::cond::QueryCondition &condition)
{
    m_queryCondition = std::move(condition);
}

bool CFunctor::GetCondition(dds::sub::cond::ReadCondition &condition)
{
    bool retVal = false;

    if (m_readCondition.is_nil() == false)
    {
        condition = m_readCondition;
        retVal = true;
    }

    return retVal;
}

void CFunctor::SetCondition(const dds::sub::cond::ReadCondition &condition)
{
    m_readCondition = std::move(condition);
}

bool CFunctor::GetCondition(dds::core::cond::StatusCondition &condition)
{
    bool retVal = false;

    if (m_statusCondition.is_nil() == false)
    {
        condition = m_statusCondition;
        retVal = true;
    }

    return retVal;
}

void CFunctor::SetCondition(const dds::core::cond::StatusCondition &condition)
{
    m_statusCondition = std::move(condition);
}

bool CFunctor::ConditionEqual(const dds::core::cond::Condition &condition)
{
    if (m_queryCondition.is_nil() == false)
    {
        if (condition == m_queryCondition)
        {
            return true;
        }
    }

    if (m_readCondition.is_nil() == false)
    {
        if (condition == m_readCondition)
        {
            return true;
        }
    }

    if (m_statusCondition.is_nil() == false)
    {
        if (condition == m_statusCondition)
        {
            return true;
        }
    }

    return false;
}

void CFunctor::operator() ()
{
    if (m_callback != nullptr)
    {
        m_callback(m_pThis);
    }
    else
    {
        std::string error("Functor Callback not set");
        DDS_FACTORY_ERROR(error);
        throw OASYS_DDS_CONNEXT_EXCEPTION(error);
    }
}

CFunctor::CFunctor(const Callback &callback) :
    m_pThis(this),
    m_callback(std::move(callback)),
    m_queryCondition(dds::core::null),
    m_readCondition(dds::core::null),
    m_statusCondition(dds::core::null)
{

}

CFunctor::CFunctor(const Callback &callback,
                   std::string expression) :
    m_pThis(this),
    m_callback(std::move(callback)),
    m_expression(expression),
    m_queryCondition(dds::core::null),
    m_readCondition(dds::core::null),
    m_statusCondition(dds::core::null)
{

}

CFunctor::~CFunctor()
{

}
