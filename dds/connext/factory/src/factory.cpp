#include <oasys/core/dds/connext/factory/factory.h>
#include <oasys/core/common/common.h>
#include "factory_macros.h"

using namespace oasys::core;
using namespace oasys::core::common;

#define FACTORY_ERROR(error) Error(DDS_FACTORY_ERROR_AT, error)

const std::string ENV_RTI_LICENSE_FILE = "RTI_LICENSE_FILE";

typedef std::lock_guard<std::mutex> LockGuard;

bool CDdsFactory::CreateDomainParticipant(dds::domain::DomainParticipant &participant,
                                          int32_t domainId,
                                          const std::string &qosProfile)
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    try
    {
        if (m_pQosProvider == nullptr)
        {
            std::string error("Cannot create DomainParticipant - DDS Factory uninitialized.");
            FACTORY_ERROR(error);
            throw OASYS_DDS_CONNEXT_EXCEPTION(error);
        }
        dds::domain::qos::DomainParticipantQos qos = m_pQosProvider->participant_qos(qosProfile);

        DomainProfileMap *pMap = &m_domainMap[domainId];
        auto it = pMap->find(qosProfile);
        if (it == pMap->end())
        {
            /// No domain participants for domain id  - Create DomainParticipant
            participant = dds::domain::DomainParticipant(domainId,
                                                         qos,
                                                         nullptr,
                                                         dds::core::status::StatusMask::all());
            DomainProfileMapPair pair(qosProfile, participant);
            pMap->insert(pair);
            retVal = true;
        }
        else
        {
            participant = it->second;
            retVal = true;
        }
    }
    DDS_EXCEPTION_CATCH_BLOCK;

    return retVal;
}

bool CDdsFactory::CreatePublisher(dds::pub::Publisher &publisher,
                                  int32_t domainId,
                                  const std::string &participantQos,
                                  const std::string &publisherQos)
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    try
    {
        /// Verify prerequisites are settled
        if (m_pQosProvider == nullptr)
        {
            std::string error("QoS Provider Uninitialized");
            FACTORY_ERROR(error);
            throw OASYS_DDS_CONNEXT_EXCEPTION(error);
        }

        auto domainIt = m_domainMap.find(domainId);
        if (domainIt == m_domainMap.end())
        {
            std::string error("Cannot create publisher for non-existent participant.");
            FACTORY_ERROR(error);
            throw OASYS_DDS_CONNEXT_EXCEPTION(error);
        }

        DomainProfileMap domainMap = domainIt->second;
        auto domainProfileIt = domainMap.find(participantQos);
        if (domainProfileIt == domainMap.end())
        {
            std::string error("Cannot create publisher for non-existent participant.");
            FACTORY_ERROR(error);
            throw OASYS_DDS_CONNEXT_EXCEPTION(error);
        }
        dds::domain::DomainParticipant domainParticipant = domainProfileIt->second;

        ///
        /// \brief Domain participant verified - Create Publisher
        ///
        dds::pub::qos::PublisherQos qos = m_pQosProvider->publisher_qos(publisherQos);

        PublisherProfileMap *pMap = &m_publisherMap[domainId];
        auto it = pMap->find(publisherQos);
        if (it == pMap->end())
        {
            /// No Publisher for domain id  - Create Publisher
            publisher = dds::pub::Publisher(domainParticipant,
                                            qos,
                                            nullptr,
                                            dds::core::status::StatusMask::all());
            PublisherProfileMapPair pair(publisherQos, publisher);
            pMap->insert(pair);
            retVal = true;
        }
        else
        {
            publisher = it->second;
            retVal = true;
        }
    }
    DDS_EXCEPTION_CATCH_BLOCK;

    return retVal;
}

bool CDdsFactory::DeletePublisher(int32_t domainId,
                                  const std::string &qosProfile)
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    try
    {
        auto pubIt = m_publisherMap.find(domainId);
        if (pubIt == m_publisherMap.end())
        {
            return false;
        }

        PublisherProfileMap *pMap = &pubIt->second;
        auto profileIt = pMap->find(qosProfile);
        if (profileIt == pMap->end())
        {
            return false;
        }
        else
        {
            dds::pub::Publisher publisher = profileIt->second;
            publisher.close();
            pMap->erase(profileIt);
            if (pMap->empty() == true)
            {
                m_publisherMap.erase(pubIt);
            }
        }
    }
    DDS_EXCEPTION_CATCH_BLOCK;

    return retVal;
}

bool CDdsFactory::CreateSubscriber(dds::sub::Subscriber &subscriber,
                                   int32_t domainId,
                                   const std::string &participantQos,
                                   const std::string &subscriberQos)
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    try
    {
        /// Verify prerequisites are settled
        if (m_pQosProvider == nullptr)
        {
            std::string error("QoS Provider Uninitialized");
            FACTORY_ERROR(error);
        }

        auto domainIt = m_domainMap.find(domainId);
        if (domainIt == m_domainMap.end())
        {
            std::string error("Cannot create subscriber for non-existent participant.");
            FACTORY_ERROR(error);
            throw OASYS_DDS_CONNEXT_EXCEPTION(error);
        }

        DomainProfileMap domainMap = domainIt->second;
        auto domainProfileIt = domainMap.find(participantQos);
        if (domainProfileIt == domainMap.end())
        {
            std::string error("Cannot create subscriber for non-existent participant.");
            FACTORY_ERROR(error);
            throw OASYS_DDS_CONNEXT_EXCEPTION(error);
        }
        dds::domain::DomainParticipant domainParticipant = domainProfileIt->second;

        /// Domain participant verified - Create Subscriber
        dds::sub::qos::SubscriberQos qos = m_pQosProvider->subscriber_qos(subscriberQos);

        SubscriberProfileMap *pMap = &m_subscriberMap[domainId];
        auto it = pMap->find(subscriberQos);
        if (it == pMap->end())
        {
            /// No Subscriber for domain id  - Create Subscriber
            subscriber = dds::sub::Subscriber(domainParticipant,
                                            qos,
                                            nullptr,
                                            dds::core::status::StatusMask::all());
            SubscriberProfileMapPair pair(subscriberQos, subscriber);
            pMap->insert(pair);
            retVal = true;
        }
        else
        {
            subscriber = it->second;
            retVal = true;
        }
    }
    DDS_EXCEPTION_CATCH_BLOCK;

    return retVal;
}

bool CDdsFactory::DeleteSubscriber(int32_t domainId,
                                   const std::string &qosProfile)
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    try
    {
        auto subIt = m_subscriberMap.find(domainId);
        if (subIt == m_subscriberMap.end())
        {
            return false;
        }

        SubscriberProfileMap *pMap = &subIt->second;
        auto profileIt = pMap->find(qosProfile);
        if (profileIt == pMap->end())
        {
            return false;
        }
        else
        {
            dds::sub::Subscriber subscriber = profileIt->second;
            subscriber.close();
            pMap->erase(profileIt);
            if (pMap->empty() == true)
            {
                m_subscriberMap.erase(subIt);
            }
        }
    }
    DDS_EXCEPTION_CATCH_BLOCK;

    return retVal;
}

bool CDdsFactory::GetWriterQos(dds::pub::qos::DataWriterQos &qos,
                               const std::string &writerQos)
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    try
    {
        /// Verify prerequisites are settled
        if (m_pQosProvider == nullptr)
        {
            std::string error("QoS Provider Uninitialized");
            FACTORY_ERROR(error);
        }

        qos = m_pQosProvider->datawriter_qos(writerQos);
        retVal = true;
    }
    DDS_EXCEPTION_CATCH_BLOCK;

    return retVal;
}

bool CDdsFactory::GetReaderQos(dds::sub::qos::DataReaderQos &qos,
                               const std::string &readerQos)
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    try
    {
        /// Verify prerequisites are settled
        if (m_pQosProvider == nullptr)
        {
            std::string error("QoS Provider Uninitialized");
        }

        qos = m_pQosProvider->datareader_qos(readerQos);
        retVal = true;
    }
    DDS_EXCEPTION_CATCH_BLOCK;

    return retVal;
}

bool CDdsFactory::GetTopicQos(dds::topic::qos::TopicQos &qos,
                              const std::string &topicQos)
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    try
    {
        if (m_pQosProvider == nullptr)
        {
            std::string error("QoS Provider Uninitialized");
            FACTORY_ERROR(error);
        }

        qos = m_pQosProvider->topic_qos(topicQos);
        retVal = true;
    }
    DDS_EXCEPTION_CATCH_BLOCK;

    return retVal;
}

void CDdsFactory::SetSignalHandler(const SignalHandlerNotification &callback)
{
    LockGuard lock(m_mutex);
    m_signalNotifier = callback;
}

bool CDdsFactory::Initialize(const std::string &qosFileUri)
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    try
    {
        VerifyLicenseEnv();
        if (m_pQosProvider == nullptr)
        {
            m_pQosProvider = new dds::core::QosProvider(qosFileUri);
        }

        dds::core::QosProvider provider = *m_pQosProvider;
        retVal = provider->profiles_loaded();
    }
    DDS_EXCEPTION_CATCH_BLOCK;

    return retVal;
}

bool CDdsFactory::Initialize(const std::string &qosFileUri,
                             const std::string &defaultProfile)
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    try
    {
        VerifyLicenseEnv();
        if (m_pQosProvider == nullptr)
        {
            m_pQosProvider = new dds::core::QosProvider(qosFileUri, defaultProfile);
        }

        dds::core::QosProvider provider = *m_pQosProvider;
        retVal = provider->profiles_loaded();
    }
    DDS_EXCEPTION_CATCH_BLOCK;

    return retVal;
}

bool CDdsFactory::Destroy()
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    try
    {
        while (m_publisherMap.empty() == false)
        {
            auto it = m_publisherMap.begin();
            PublisherProfileMap *pMap = &it->second;

            while (pMap->empty() == false)
            {
                auto profileIt = pMap->begin();
                dds::pub::Publisher *pPublisher = &profileIt->second;
                pPublisher->close();
                pMap->erase(profileIt);
            }

            m_publisherMap.erase(it);
        }

        while (m_subscriberMap.empty() == false)
        {
            auto it = m_subscriberMap.begin();
            SubscriberProfileMap *pMap = &it->second;

            while (pMap->empty() == false)
            {
                auto profileIt = pMap->begin();
                dds::sub::Subscriber *pSubscriber = &profileIt->second;
                pSubscriber->close();
                pMap->erase(profileIt);
            }

            m_subscriberMap.erase(it);
        }

        while (m_domainMap.empty() == false)
        {
            auto it = m_domainMap.begin();
            DomainProfileMap *pMap = &it->second;

            while (pMap->empty() == false)
            {
                auto profileIt = pMap->begin();
                dds::domain::DomainParticipant *pParticipant = &profileIt->second;
                pParticipant->close();
                pMap->erase(profileIt);
            }

            m_domainMap.erase(it);
        }

        if (m_pQosProvider != nullptr)
        {
            delete m_pQosProvider;
            m_pQosProvider = nullptr;
        }

    }
    DDS_EXCEPTION_CATCH_BLOCK;

    return retVal;
}

void CDdsFactory::RegisterSignalHandler()
{
    LockGuard lock(m_mutex);

    if (m_signalNotifier != nullptr)
    {
        m_signalNotifier();
    }
    else
    {
///TODo        std::string error("Failed to notify new entity - signal handler null");
//        FACTORY_ERROR(error);
    }
}

bool CDdsFactory::RegisterErrorDelegate(ErrorDelegate &delegate)
{
    LockGuard lock(m_delegateMutex);
    return RegisterSubscriber<ErrorVector, ErrorDelegate>(m_errorVector, delegate);
}

bool oasys::core::CDdsFactory::UnregisterErrorDelegate(OwnerSharedPtr pOwner)
{
    LockGuard lock(m_delegateMutex);
    return UnregisterSubscriber<ErrorVector>(m_errorVector, pOwner);
}

void CDdsFactory::Error(const char *location,
                        const std::string &message)
{
    LockGuard lock(m_delegateMutex);

    std::string error(location);
    error.append(" Error: " + message);
    if (m_errorVector.empty() == false)
    {
        ProcessErrorHandlers(message);
    }
    else
    {
        printf("\n%s\n", error.c_str());
    }
}

void oasys::core::CDdsFactory::Error(const char *location,
                                     const std::string &topic,
                                     const std::string &message)
{
    LockGuard lock(m_delegateMutex);

    std::string error(location);
    error.append(" Topic ["+topic+"] Message["+message+"]");
    if (m_errorVector.empty() == false)
    {
        ProcessErrorHandlers(error);
    }
    else
    {
        printf("\n%s\n", error.c_str());
    }
}

CDdsFactory::CDdsFactory() :
    m_pQosProvider(nullptr),
    m_signalNotifier(nullptr)
{

}

CDdsFactory::~CDdsFactory()
{
    /// Ensure factory has safely shutdown and closed all DDS entities.
    Destroy();
}

bool CDdsFactory::VerifyLicenseEnv()
{
    bool retVal = false;

    char *pLicense = getenv(ENV_RTI_LICENSE_FILE.c_str());
    if (pLicense == nullptr)
    {
        std::string error("RTI License File has not been defined using ENV [RTI_LICENSE_FILE]");
        FACTORY_ERROR(error);
        throw OASYS_DDS_CONNEXT_EXCEPTION(error);
    }
    else
    {
        retVal = true;
    }

    return retVal;
}

void CDdsFactory::ProcessErrorHandlers(const std::string &message)
{
    for (auto it = m_errorVector.begin(); it != m_errorVector.end(); it++)
    {
        it->Execute(message);
    }
}
