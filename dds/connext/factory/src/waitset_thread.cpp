#include <oasys/core/dds/connext/factory/waitset_thread.h>
#include "exception.h"

const int64_t DEFAULT_SLEEP_MS = 20;
const uint32_t DEFAULT_TIMEOUT_SEC = 10;
const uint32_t DEFAULT_TIMEOUT_NANOSEC = 0;

typedef std::lock_guard<std::mutex> LockGuard;

using namespace oasys::core;

bool CWaitsetThread::RegisterCondition(CFunctor * const pFunctor)
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    auto it = FindFunctor(pFunctor);
    if (it == m_functorVector.end())
    {
        m_functorVector.push_back(pFunctor);
        m_updateCondition.trigger_value(true);
        retVal = true;
    }

    return retVal;
}

bool CWaitsetThread::UnregisterCondition(const dds::core::cond::Condition &condition)
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    auto it = FindFunctor(condition);
    if (it != m_functorVector.end())
    {
        m_functorVector.erase(it);
        m_updateCondition.trigger_value(true);
        retVal = true;
    }

    return retVal;
}

void CWaitsetThread::Timeout(dds::core::Duration duration)
{
    LockGuard lock(m_mutexMembers);
    m_timeout = duration;
    m_updateCondition.trigger_value(true);
}

dds::core::Duration CWaitsetThread::Timeout()
{
    dds::core::Duration duration;

    LockGuard lock(m_mutexMembers);
    duration = m_timeout;

    return duration;
}

bool CWaitsetThread::Running()
{
    bool running = false;

    LockGuard lock(m_mutexMembers);
    running = m_running;
    return running;
}

void CWaitsetThread::Start()
{
    if (Running() == false)
    {
        m_stopCondition.trigger_value(false);
        m_updateCondition.trigger_value(true);
        m_thread = std::thread(&CWaitsetThread::Run, this);
    }
}

bool CWaitsetThread::Stop()
{
    bool retVal = false;

    m_stopCondition.trigger_value(true);

    //Allow thread to finish processing before join.
    while (Running() == true)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(DEFAULT_SLEEP_MS));
    }
    if ((m_threadMutex.try_lock() == true) && (m_thread.joinable() == true))
    {
        m_thread.join();
    }
    Destroy();
    retVal = true;

    return retVal;
}

void CWaitsetThread::Destroy()
{
    LockGuard lock(m_mutex);

    DestroyWaitSet();
    m_functorVector.clear();
}

CWaitsetThread::CWaitsetThread(const std::string topicName) :
    m_running(false),
    m_topicName(topicName)
{
    m_timeout.sec(DEFAULT_TIMEOUT_SEC);
    m_timeout.nanosec(DEFAULT_TIMEOUT_NANOSEC);
}

CWaitsetThread::~CWaitsetThread()
{

}

void CWaitsetThread::Run()
{
    SetRunning(true);
    try
    {
        while (m_stopCondition.trigger_value() == false)
        {
            // Run if no pending updates. If pending update, reconfigure then run.
            bool run = !m_updateCondition.trigger_value();

            if (run == false)
            {
                try
                {
                    run = SetupWaitset();
                }
                catch (const dds::core::Exception& e)
                {
                    std::string error("Failed to configure waitset ["+std::string(e.what())+"]");
                    DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
                    throw CDdsException(m_topicName, error);
                }
            }

            if (run == true)
            {
                try
                {
                    m_waitSet.dispatch();
                }
                catch (const dds::core::TimeoutError &e)
                {
                }
                catch (const dds::core::Exception& e)
                {
                    std::string error("Waitset dispatch expception: " + std::string(e.what()));
                    DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
                    throw CDdsException(m_topicName, error);
                }
            }
            else
            {
                std::string error("Failed to configure waitset");
                DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
                throw CDdsException(m_topicName, error);
            }
        }

    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        std::string error(e.what());
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
    }
    SetRunning(false);
    // if the mutex is locked, the thread is just bout to join
    if (m_threadMutex.try_lock() == true)
    {
        m_thread.detach();
    }
}

void CWaitsetThread::SetRunning(bool running)
{
    LockGuard lock(m_mutexMembers);
    m_running = running;
}

bool CWaitsetThread::SetupWaitset()
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    if (m_waitSet.is_nil() == false)
    {
        DestroyWaitSet();

        m_waitSet.attach_condition(m_stopCondition);

        m_waitSet.attach_condition(m_updateCondition);

        for (auto it = m_functorVector.begin(); it != m_functorVector.end(); it++)
        {
            CFunctor *pFunctor = *it;
            m_waitSet.attach_condition(pFunctor->GetCondition());
        }

        retVal = true;
        m_updateCondition.trigger_value(false);
    }
    else
    {
        std::string error("Invalid waitset [null]");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }

    return retVal;
}

void CWaitsetThread::DestroyWaitSet()
{
    try
    {
        dds::core::cond::WaitSet::ConditionSeq seq = m_waitSet.conditions();
        for (auto it = seq.begin(); it != seq.end(); it++)
        {
            m_waitSet.detach_condition(*it);
        }
    }
    catch (const dds::core::Exception& e)
    {
        std::string error("Failed to destroy waitset["+std::string(e.what())+"]");
        DDS_FACTORY_TOPIC_ERROR(m_topicName, error);
        throw CDdsException(m_topicName, error);
    }
}

CWaitsetThread::FunctorIt CWaitsetThread::FindFunctor(const dds::core::cond::Condition &condition)
{
    for (auto it = m_functorVector.begin(); it != m_functorVector.end(); it++)
    {
        CFunctor *pFunctor = *it;
        if (pFunctor->ConditionEqual(condition))
        {
            return it;
        }
    }

    return m_functorVector.end();
}

CWaitsetThread::FunctorIt CWaitsetThread::FindFunctor(CFunctor * const pFunctor)
{
    for (auto it = m_functorVector.begin(); it != m_functorVector.end(); it++)
    {
        if (pFunctor == *it)
        {
            return it;
        }
    }

    return m_functorVector.end();
}
