/* test.h
 *
 * This file contains generated code. Do not modify!
 */

#ifndef TEST_H
#define TEST_H

#include <corto/corto.h>
#include <corto/corto.h>
#include <include/_project.h>
#include <corto/test/test.h>
#include <corto/c/c.h>
#include <oasys/core/dds/connext/models/foo/foo.h>
#include <oasys/core/dds/connext/factory/factory.h>

/* $header() */

const std::string QOS_URI = "/usr/local/etc/corto/1.1/oasys/core/dds/connext/factory/qos.xml";
const std::string TRANSIENT_PROFILE = "OASYS::TransientKeepLast";
const std::string BEST_EFFORT_PROFILE = "OASYS::BestEffort";
const std::string QOS_URI_BAD = "qos_bad.xml";
const std::string BEST_EFFORT_BAD_FORMAT = "BestEffort";

const uint32_t DEFAULT_DOMAIN_ID = 0;

using namespace oasys::core;
using namespace oasys::core::common;

/* $end */

#include <include/_type.h>
#include <include/_interface.h>
#include <include/_load.h>
#include <include/_api.h>

/* $body() */
/* Enter code that requires types here */
/* $end */

#endif

