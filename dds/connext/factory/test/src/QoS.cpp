/* $CORTO_GENERATED
 *
 * QoS.cpp
 *
 * Only code written between the begin and end tags will be preserved
 * when the file is regenerated.
 */

#include <include/test.h>

/* $header() */

CDdsFactory *pQosDdsFactory;

void QosInitialize(void)
{
    pQosDdsFactory = DdsFactory::GetInstance();
    if (pQosDdsFactory != nullptr)
    {
        pQosDdsFactory->Initialize(QOS_URI);
    }

    dds::domain::DomainParticipant participant(dds::core::null);
    pQosDdsFactory->CreateDomainParticipant(participant,
                                         DEFAULT_DOMAIN_ID,
                                         BEST_EFFORT_PROFILE);
}

/* $end */

void _test_QoS_VerifyBad(
    test_QoS _this)
{
/* $begin(test/QoS/VerifyBad) */

    bool retVal = false;
    bool qosVerified = false;
    try
    {
        QosInitialize();
        dds::pub::Publisher publisher(dds::core::null);
        retVal = pQosDdsFactory->CreatePublisher(publisher,
                                              DEFAULT_DOMAIN_ID,
                                              BEST_EFFORT_PROFILE,
                                              BEST_EFFORT_PROFILE);

        dds::pub::qos::PublisherQos qos = publisher.qos();
        dds::core::policy::Presentation policy = qos->presentation;

        bool orderedAccess = policy.ordered_access();

        if (orderedAccess == false)
        {
            qosVerified = true;
        }

        if (retVal == true)
        {
            pQosDdsFactory->DeletePublisher(DEFAULT_DOMAIN_ID,
                                         BEST_EFFORT_PROFILE);
        }
        else
        {
            retVal = false;
        }

        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        retVal = false;
    }

    test_assert(qosVerified == false);
    test_assert(retVal == true);

/* $end */
}

void _test_QoS_VerifyGood(
    test_QoS _this)
{
/* $begin(test/QoS/VerifyGood) */

    bool retVal = false;
    bool qosVerified = false;
    try
    {
        QosInitialize();
        dds::pub::Publisher publisher(dds::core::null);
        retVal = pQosDdsFactory->CreatePublisher(publisher,
                                              DEFAULT_DOMAIN_ID,
                                              BEST_EFFORT_PROFILE,
                                              BEST_EFFORT_PROFILE);

        dds::pub::qos::PublisherQos qos = publisher.qos();
        dds::core::policy::Presentation policy = qos->presentation;

        bool orderedAccess = policy.ordered_access();

        if (orderedAccess == true)
        {
            qosVerified = true;
        }

        if (retVal == true)
        {
            pQosDdsFactory->DeletePublisher(DEFAULT_DOMAIN_ID,
                                         BEST_EFFORT_PROFILE);
        }
        else
        {
            retVal = false;
        }

        DdsFactory::Destroy();

    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        retVal = false;
    }

    test_assert(qosVerified == true);
    test_assert(retVal == true);
/* $end */
}
