/* $CORTO_GENERATED
 *
 * ReaderDataAccess.cpp
 *
 * Only code written between the begin and end tags will be preserved
 * when the file is regenerated.
 */

#include <include/test.h>

/* $header() */
#include <oasys/core/dds/connext/factory/foo_reader.h>
#include <oasys/core/dds/connext/factory/foo_writer.h>
#include <oasys/core/dds/connext/models/foo/Foo.hpp>

using namespace oasys::core;

using namespace oasys::core::interfaces::foo;
/* $end */

void _test_ReaderDataAccess_Query(
    test_ReaderDataAccess _this)
{
/* $begin(test/ReaderDataAccess/Query) */


    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();
        CFooWriter *pWriter = new CFooWriter();

        if ((pReader->Initialize() == false) || (pWriter->Initialize() == false))
        {
            test_assert(false);
        }

        int x = 0;
        int y = 4;

        for (int i = 0; i < 10; i++)
        {
            x++;
            Foo foo(x, y);
            if (pWriter->Write(foo) == false)
            {
                test_assert(false);
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(20));

        std::string expression ("x = 5 AND y = 4");
        CFooReader::SampleSeq readSamples = pReader->Query(expression);

        if (readSamples.size() == 0)
        {
            test_assert(false);
        }

        CFooReader::Sample readData = readSamples[0];
        if (readData.info().valid() == false)
        {
            test_assert(false);
        }

        if ((readData.data().x() == 5) && (readData.data().y() == 4))
        {
            printf("\nRead Success\n"
                   "x:%d - read:%d\n"
                   "y:%d - read:%d\n",
                   x, readData.data().x(),
                   y, readData.data().y());
            retVal = true;
        }
        else
        {
            printf("\nRead Failed\n"
                   "x:%d - read:%d\n"
                   "y:%d - read:%d\n",
                   x, readData.data().x(),
                   y, readData.data().y());
            test_assert(false);
        }
        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }
        if (pWriter != nullptr)
        {
            pWriter->Destroy();
            delete pWriter;
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }

    test_assert(retVal == true);

/* $end */
}

void _test_ReaderDataAccess_QueryParams(
    test_ReaderDataAccess _this)
{
/* $begin(test/ReaderDataAccess/QueryParams) */

    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();
        CFooWriter *pWriter = new CFooWriter();

        if ((pReader->Initialize() == false) || (pWriter->Initialize() == false))
        {
            test_assert(false);
        }

        int x = 0;
        int y = 4;

        for (int i = 0; i < 10; i++)
        {
            x++;
            Foo foo(x, y);
            if (pWriter->Write(foo) == false)
            {
                test_assert(false);
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(20));

        CFooReader::ParamVector params(2);
        params[0] = "5";
        params[1] = "4";
        std::string expression ("x = %0 AND y = %1");
        CFooReader::SampleSeq readSamples = pReader->Query(expression, params);

        if (readSamples.size() == 0)
        {
            test_assert(false);
        }

        CFooReader::Sample readData = readSamples[0];
        if (readData.info().valid() == false)
        {
            test_assert(false);
        }

        if ((readData.data().x() == 5) && (readData.data().y() == 4))
        {
            printf("\nRead Success\n"
                   "x:%d - read:%d\n"
                   "y:%d - read:%d\n",
                   x, readData.data().x(),
                   y, readData.data().y());
            retVal = true;
        }
        else
        {
            printf("\nRead Failed\n"
                   "x:%d - read:%d\n"
                   "y:%d - read:%d\n",
                   x, readData.data().x(),
                   y, readData.data().y());
            test_assert(false);
        }
        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }
        if (pWriter != nullptr)
        {
            pWriter->Destroy();
            delete pWriter;
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }

    test_assert(retVal == true);

/* $end */
}

void _test_ReaderDataAccess_Read(
    test_ReaderDataAccess _this)
{
/* $begin(test/ReaderDataAccess/Read) */

    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();
        CFooWriter *pWriter = new CFooWriter();

        if ((pReader->Initialize() == false) || (pWriter->Initialize() == false))
        {
            test_assert(false);
        }

        int x = 0;
        int y = 4;

        while (x < 10)
        {
            Foo foo(x, y);
            if (pWriter->Write(foo) == false)
            {
                test_assert(false);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(20));

            CFooReader::Sample readData = pReader->Read(READ_END);

            if (readData.info().valid() == false)
            {
                test_assert(false);
            }

            if ((readData.data().x() == x) && (readData.data().y() == y))
            {
                printf("\nRead Success\n"
                       "x:%d - read:%d\n"
                       "y:%d - read:%d\n",
                       x, readData.data().x(),
                       y, readData.data().y());
                retVal = true;
            }
            else
            {
                printf("\nRead Failed\n"
                       "x:%d - read:%d\n"
                       "y:%d - read:%d\n",
                       x, readData.data().x(),
                       y, readData.data().y());
                test_assert(false);
            }
            x++;
            y++;
        }

        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }
        if (pWriter != nullptr)
        {
            pWriter->Destroy();
            delete pWriter;
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }

    test_assert(retVal == true);

/* $end */
}

void _test_ReaderDataAccess_ReadBegin(
    test_ReaderDataAccess _this)
{
/* $begin(test/ReaderDataAccess/ReadBegin) */

    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();
        CFooWriter *pWriter = new CFooWriter();

        if ((pReader->Initialize() == false) || (pWriter->Initialize() == false))
        {
            test_assert(false);
        }

        int xFirst = 0;
        int yFirst = 4;
        int x = 0;
        int y = 4;

        while (x < 10)
        {
            Foo foo(x, y);
            if (pWriter->Write(foo) == false)
            {
                test_assert(false);
            }

            CFooReader::Sample readData = pReader->Read(READ_BEGIN);

            if (readData.info().valid() == false)
            {
                test_assert(false);
            }

            if ((readData.data().x() == xFirst) && (readData.data().y() == yFirst))
            {
                printf("\nRead Success\n"
                       "x:%d - read:%d\n"
                       "y:%d - read:%d\n",
                       x, readData.data().x(),
                       y, readData.data().y());
                retVal = true;
            }
            else
            {
                printf("\nRead Failed\n"
                       "x:%d - read:%d\n"
                       "y:%d - read:%d\n",
                       x, readData.data().x(),
                       y, readData.data().y());
                test_assert(false);
            }

            x++;
            y++;
        }

        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }
        if (pWriter != nullptr)
        {
            pWriter->Destroy();
            delete pWriter;
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }

    test_assert(retVal == true);
/* $end */
}

void _test_ReaderDataAccess_ReadNoData(
    test_ReaderDataAccess _this)
{
/* $begin(test/ReaderDataAccess/ReadNoData) */

    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();

        if (pReader->Initialize() == false)
        {
            test_assert(false);
        }

        CFooReader::Sample readData = pReader->Read(READ_BEGIN);

        if (readData.info().valid() == false)
        {
            printf("No Data received.\n");
            retVal = true;
        }

        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }

        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }

    test_assert(retVal == true);
/* $end */
}

void _test_ReaderDataAccess_ReadSeq(
    test_ReaderDataAccess _this)
{
/* $begin(test/ReaderDataAccess/ReadSeq) */

    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();
        CFooWriter *pWriter = new CFooWriter();

        if ((pReader->Initialize() == false) || (pWriter->Initialize() == false))
        {
            test_assert(false);
        }

        int x = 0;
        int y = 4;
        int sum = 0;

        while (x < 10)
        {
            Foo foo(x, y);
            if (pWriter->Write(foo) == false)
            {
                test_assert(false);
            }

            printf("Sum [%d] x=%d y=%d\n", sum, x, y);
            sum = sum + x + y;
            x++;
            y++;
        }

        CFooReader::SampleSeq readData = pReader->ReadSeq();

        if (readData.size() > 0)
        {
            printf("Received [%zd] samples\n", readData.size());
            int readSum = 0;
            for (auto it = readData.begin(); it != readData.end(); it++)
            {
                CFooReader::Sample sample(*it);
                if (sample.info().valid() == false)
                {
                    test_assert(false);
                }
                printf("Sum [%d] x=%d y=%d\n", readSum, sample.data().x(), sample.data().y());
                readSum = readSum + sample.data().x() + sample.data().y();
            }

            if (readSum == sum)
            {
                printf("\nRead Success\n"
                       "Sum:%d - read:%d\n",
                       sum, readSum);
                retVal = true;
            }
            else
            {
                printf("\nRead Failed\n"
                       "Sum:%d - read:%d\n",
                       sum, readSum);
                test_assert(false);
            }
        }
        else
        {
            test_assert(false);
        }


        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }
        if (pWriter != nullptr)
        {
            pWriter->Destroy();
            delete pWriter;
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (dds::core::Exception &e)
    {
        printf("\n\nReceived unhandled exception[%s]\n\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }


    test_assert(retVal == true);
/* $end */
}

void _test_ReaderDataAccess_Take(
    test_ReaderDataAccess _this)
{
/* $begin(test/ReaderDataAccess/Take) */

    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();
        CFooWriter *pWriter = new CFooWriter();

        if ((pReader->Initialize() == false) || (pWriter->Initialize() == false))
        {
            test_assert(false);
        }

        int x = 0;
        int y = 4;

        while (x < 10)
        {
            Foo foo(x, y);
            if (pWriter->Write(foo) == false)
            {
                test_assert(false);
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(20));

            CFooReader::Sample readData = pReader->Take();

            if (readData.info().valid() == false)
            {
                test_assert(false);
            }

            if ((readData.data().x() == x) && (readData.data().y() == y))
            {
                printf("\nRead Success\n"
                       "x:%d - read:%d\n"
                       "y:%d - read:%d\n",
                       x, readData.data().x(),
                       y, readData.data().y());
                retVal = true;
            }
            else
            {
                printf("\nRead Failed\n"
                       "x:%d - read:%d\n"
                       "y:%d - read:%d\n",
                       x, readData.data().x(),
                       y, readData.data().y());
                test_assert(false);
            }

            x++;
            y++;
        }

        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }
        if (pWriter != nullptr)
        {
            pWriter->Destroy();
            delete pWriter;
        }
        DdsFactory::Destroy();

    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }

    test_assert(retVal == true);

/* $end */
}

void _test_ReaderDataAccess_TakeSeq(
    test_ReaderDataAccess _this)
{
/* $begin(test/ReaderDataAccess/TakeSeq) */

    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();
        CFooWriter *pWriter = new CFooWriter();

        if ((pReader->Initialize() == false) || (pWriter->Initialize() == false))
        {
            test_assert(false);
        }

        int x = 0;
        int y = 4;
        int sum = 0;

        while (x < 10)
        {
            Foo foo(x, y);
            if (pWriter->Write(foo) == false)
            {
                test_assert(false);
            }

            printf("Sum [%d] x=%d y=%d\n", sum, x, y);
            sum = sum + x + y;
            x++;
            y++;
        }

        CFooReader::SampleSeq readData = pReader->TakeSeq();

        if (readData.size() > 0)
        {
            printf("Received [%zd] samples\n", readData.size());
            int readSum = 0;
            for (auto it = readData.begin(); it != readData.end(); it++)
            {
                CFooReader::Sample sample(*it);
                if (sample.info().valid() == false)
                {
                    test_assert(false);
                }
                printf("Sum [%d] x=%d y=%d\n", readSum, sample.data().x(), sample.data().y());
                readSum = readSum + sample.data().x() + sample.data().y();
            }
        }
        else
        {
            test_assert(false);
        }

        CFooReader::SampleSeq readData2 = pReader->TakeSeq();

        if (readData2.size() == 0)
        {
            retVal = true;
        }
        else
        {
            test_assert(false);
        }

        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }
        if (pWriter != nullptr)
        {
            pWriter->Destroy();
            delete pWriter;
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (dds::core::Exception &e)
    {
        printf("\n\nReceived unhandled exception[%s]\n\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }


    test_assert(retVal == true);

/* $end */
}
