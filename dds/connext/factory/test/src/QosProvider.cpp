/* $CORTO_GENERATED
 *
 * QosProvider.cpp
 *
 * Only code written between the begin and end tags will be preserved
 * when the file is regenerated.
 */

#include <include/test.h>

/* $header() */

/* $end */

void _test_QosProvider_BadFile(
    test_QosProvider _this)
{
/* $begin(test/QosProvider/BadFile) */

    bool retVal = false;

    CDdsFactory *pDdsFactory = DdsFactory::GetInstance();
    if (pDdsFactory != nullptr)
    {
        try
        {
            pDdsFactory->Initialize(QOS_URI_BAD);
            DdsFactory::Destroy();
        }
        catch (OASYS_DDS_CONNEXT_EXCEPTION &excpetion)
        {
            retVal = true;
        }
    }

    DdsFactory::Destroy();

    test_assert(retVal == true);
/* $end */
}

void _test_QosProvider_BadFormat(
    test_QosProvider _this)
{
/* $begin(test/QosProvider/BadFormat) */


    bool retVal = false;

    CDdsFactory *pDdsFactory = DdsFactory::GetInstance();
    if (pDdsFactory != nullptr)
    {
        try
        {
            pDdsFactory->Initialize(QOS_URI, BEST_EFFORT_BAD_FORMAT);
            DdsFactory::Destroy();
        }
        catch (OASYS_DDS_CONNEXT_EXCEPTION &excpetion)
        {
            retVal = true;
        }
    }

    DdsFactory::Destroy();

    test_assert(retVal == true);
/* $end */
}

void _test_QosProvider_Create(
    test_QosProvider _this)
{
/* $begin(test/QosProvider/Create) */

    bool initBasic = false;
    bool initTKL = false;
    bool initBE = false;
    CDdsFactory *pDdsFactory1 = DdsFactory::GetInstance();
    if (pDdsFactory1 != nullptr)
    {
        initBasic = pDdsFactory1->Initialize(QOS_URI);
        DdsFactory::Destroy();
    }

    CDdsFactory *pDdsFactory2 = DdsFactory::GetInstance();
    if (pDdsFactory2 != nullptr)
    {
        initTKL = pDdsFactory2->Initialize(QOS_URI, TRANSIENT_PROFILE);
        DdsFactory::Destroy();
    }

    CDdsFactory *pDdsFactory3 = DdsFactory::GetInstance();
    if (pDdsFactory3 != nullptr)
    {
        initBE = pDdsFactory3->Initialize(QOS_URI, BEST_EFFORT_PROFILE);
        DdsFactory::Destroy();
    }

    DdsFactory::Destroy();

    test_assert(initBasic == true);
    test_assert(initTKL == true);
    test_assert(initBE == true);
/* $end */
}
