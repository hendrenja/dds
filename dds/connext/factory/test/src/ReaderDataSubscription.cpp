/* $CORTO_GENERATED
 *
 * ReaderDataSubscription.cpp
 *
 * Only code written between the begin and end tags will be preserved
 * when the file is regenerated.
 */

#include <include/test.h>

/* $header() */
#include <oasys/core/dds/connext/factory/foo_reader.h>
#include <oasys/core/dds/connext/factory/foo_writer.h>
#include <oasys/core/dds/connext/models/foo/Foo.hpp>

using namespace oasys::core;

using namespace oasys::core::interfaces::foo;
/* $end */

void _test_ReaderDataSubscription_RegisterData(
    test_ReaderDataSubscription _this)
{
/* $begin(test/ReaderDataSubscription/RegisterData) */

    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();
        CFooWriter *pWriter = new CFooWriter();

        if ((pReader->Initialize() == false) || (pWriter->Initialize() == false))
        {
            test_assert(false);
        }

        int x = 0;
        int y = 4;

        std::shared_ptr<int> pInt(new int(10));

        CFooReader::DataDelegate::Callback callback = [&] (CFooReader::Sample sample)
        {
            if (sample.info().valid() == true)
            {
                if ((sample.data().x() == x) && (sample.data().y() == y))
                {
                    printf("\nRead Success\n"
                           "x:%d - read:%d\n"
                           "y:%d - read:%d\n",
                           x, sample.data().x(),
                           y, sample.data().y());
                    retVal = true;
                }
            }
            else
            {
                test_assert(false);
            }
        };
        CFooReader::DataDelegate delegate(callback, pInt);
        if (pReader->RegisterNewDataSubscriber(delegate) == false)
        {
            test_assert(false);
        }

        Foo foo(x, y);
        if (pWriter->Write(foo) == false)
        {
            test_assert(false);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(20));


        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }
        if (pWriter != nullptr)
        {
            pWriter->Destroy();
            delete pWriter;
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }


    test_assert(retVal == true);

/* $end */
}

void _test_ReaderDataSubscription_RegisterNotAlive(
    test_ReaderDataSubscription _this)
{
/* $begin(test/ReaderDataSubscription/RegisterNotAlive) */

    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();
        CFooWriter *pWriter = new CFooWriter();

        if ((pReader->Initialize() == false) || (pWriter->Initialize() == false))
        {
            test_assert(false);
        }

        int x = 0;
        int y = 4;

        std::shared_ptr<int> pInt(new int(10));

        CFooReader::DataDelegate::Callback callback = [&] (CFooReader::Sample sample)
        {
            if (sample.info().valid() == true)
            {
                if ((sample.data().x() == x) && (sample.data().y() == y))
                {
                    printf("\nRead Success\n"
                           "x:%d - read:%d\n"
                           "y:%d - read:%d\n",
                           x, sample.data().x(),
                           y, sample.data().y());
                    retVal = true;
                }
            }
            else
            {
                test_assert(false);
            }
        };
        CFooReader::DataDelegate delegate(callback, pInt);
        if (pReader->RegisterInstanceNotAliveSubscriber(delegate) == false)
        {
            test_assert(false);
        }

        Foo foo(x, y);
        dds::core::InstanceHandle handle = pWriter->RegisterInstance(foo);
        if (pWriter->Write(foo, handle) == false)
        {
            test_assert(false);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(20));

        if (pWriter->UnregisterInstance(handle, true) == false)
        {
            test_assert(false);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(20));


        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }
        if (pWriter != nullptr)
        {
            pWriter->Destroy();
            delete pWriter;
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }


    test_assert(retVal == true);
/* $end */
}

void _test_ReaderDataSubscription_RegisterQuery(
    test_ReaderDataSubscription _this)
{
/* $begin(test/ReaderDataSubscription/RegisterQuery) */

    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();
        CFooWriter *pWriter = new CFooWriter();

        if ((pReader->Initialize() == false) || (pWriter->Initialize() == false))
        {
            test_assert(false);
        }

        int x = 0;
        int y = 4;

        std::shared_ptr<int> pInt(new int(10));

        CFooReader::DataSeqDelegate::Callback callback = [&] (CFooReader::SampleSeq samples)
        {
            if (samples.size() > 0)
            {
                CFooReader::Sample sample = *samples.begin();
                if (sample.info().valid() == true)
                {
                    if ((sample.data().x() == x) && (sample.data().y() == y))
                    {
                        printf("\nRead Success\n"
                               "x:%d - read:%d\n"
                               "y:%d - read:%d\n",
                               x, sample.data().x(),
                               y, sample.data().y());
                        retVal = true;
                    }
                }
                else
                {
                    test_assert(false);
                }
            }
        };

        CFooReader::ParamVector params(2);
        params[0] = "0";
        params[1] = "4";
        std::string expression ("x = %0 AND y = %1");

        CFooReader::DataSeqDelegate delegate(callback, pInt);
        if (pReader->RegisterQuery(expression, params, delegate) == false)
        {
            test_assert(false);
        }

        Foo foo(x, y);
        if (pWriter->Write(foo) == false)
        {
            test_assert(false);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(20));


        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }
        if (pWriter != nullptr)
        {
            pWriter->Destroy();
            delete pWriter;
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }


    test_assert(retVal == true);

/* $end */
}

void _test_ReaderDataSubscription_UnregisterData(
    test_ReaderDataSubscription _this)
{
/* $begin(test/ReaderDataSubscription/UnregisterData) */

    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();
        CFooWriter *pWriter = new CFooWriter();

        if ((pReader->Initialize() == false) || (pWriter->Initialize() == false))
        {
            test_assert(false);
        }

        int x = 0;
        int y = 4;

        std::shared_ptr<int> pInt(new int(10));

        CFooReader::DataDelegate::Callback callback = [&] (CFooReader::Sample sample)
        {
        };
        CFooReader::DataDelegate delegate(callback, pInt);
        if (pReader->RegisterNewDataSubscriber(delegate) == false)
        {
            test_assert(false);
        }
        retVal = pReader->UnregisterNewDataSubscriber(pInt);

        Foo foo(x, y);
        if (pWriter->Write(foo) == false)
        {
            test_assert(false);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(20));


        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }
        if (pWriter != nullptr)
        {
            pWriter->Destroy();
            delete pWriter;
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }


    test_assert(retVal == true);
/* $end */
}

void _test_ReaderDataSubscription_UnregisterNotAlive(
    test_ReaderDataSubscription _this)
{
/* $begin(test/ReaderDataSubscription/UnregisterNotAlive) */

    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();
        CFooWriter *pWriter = new CFooWriter();

        if ((pReader->Initialize() == false) || (pWriter->Initialize() == false))
        {
            test_assert(false);
        }

        int x = 0;
        int y = 4;

        std::shared_ptr<int> pInt(new int(10));

        CFooReader::DataDelegate::Callback callback = [&] (CFooReader::Sample sample)
        {
        };
        CFooReader::DataDelegate delegate(callback, pInt);
        if (pReader->RegisterInstanceNotAliveSubscriber(delegate) == false)
        {
            test_assert(false);
        }
        retVal = pReader->UnregisterInstanceNotAliveSubscriber(pInt);

        Foo foo(x, y);
        dds::core::InstanceHandle handle = pWriter->RegisterInstance(foo);
        if (pWriter->Write(foo, handle) == false)
        {
            test_assert(false);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(20));

        if (pWriter->UnregisterInstance(handle, true) == false)
        {
            test_assert(false);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(20));


        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }
        if (pWriter != nullptr)
        {
            pWriter->Destroy();
            delete pWriter;
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }


    test_assert(retVal == true);

/* $end */
}

void _test_ReaderDataSubscription_UnregisterQuery(
    test_ReaderDataSubscription _this)
{
/* $begin(test/ReaderDataSubscription/UnregisterQuery) */

    bool retVal = false;

    try
    {
        CFooReader *pReader = new CFooReader();
        CFooWriter *pWriter = new CFooWriter();

        if ((pReader->Initialize() == false) || (pWriter->Initialize() == false))
        {
            test_assert(false);
        }

        int x = 0;
        int y = 4;

        std::shared_ptr<int> pInt(new int(10));

        CFooReader::DataSeqDelegate::Callback callback = [&] (CFooReader::SampleSeq samples)
        {
        };

        CFooReader::ParamVector params(2);
        params[0] = "0";
        params[1] = "4";
        std::string expression ("x = %0 AND y = %1");

        CFooReader::DataSeqDelegate delegate(callback, pInt);
        if (pReader->RegisterQuery(expression, params, delegate) == false)
        {
            test_assert(false);
        }

        Foo foo(x, y);
        if (pWriter->Write(foo) == false)
        {
            test_assert(false);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(20));

        retVal = pReader->UnRegisterQuery(expression, pInt);

        if (pReader != nullptr)
        {
            pReader->Destroy();
            delete pReader;
        }
        if (pWriter != nullptr)
        {
            pWriter->Destroy();
            delete pWriter;
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }


    test_assert(retVal == true);

/* $end */
}
