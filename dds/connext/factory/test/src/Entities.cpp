/* $CORTO_GENERATED
 *
 * Entities.cpp
 *
 * Only code written between the begin and end tags will be preserved
 * when the file is regenerated.
 */

#include <include/test.h>

/* $header() */

CDdsFactory *pDdsFactory;

void Initialize(void)
{
    pDdsFactory = DdsFactory::GetInstance();
    if (pDdsFactory != nullptr)
    {
        pDdsFactory->Initialize(QOS_URI);
    }

    dds::domain::DomainParticipant participant(dds::core::null);
    pDdsFactory->CreateDomainParticipant(participant,
                                         DEFAULT_DOMAIN_ID,
                                         BEST_EFFORT_PROFILE);
}

/* $end */

void _test_Entities_CreatePublisher(
    test_Entities _this)
{
/* $begin(test/Entities/CreatePublisher) */

    bool retVal = false;

    try
    {
        Initialize();
        dds::pub::Publisher publisher(dds::core::null);
        retVal = pDdsFactory->CreatePublisher(publisher,
                                              DEFAULT_DOMAIN_ID,
                                              BEST_EFFORT_PROFILE,
                                              BEST_EFFORT_PROFILE);

        if (retVal == true)
        {
            pDdsFactory->DeletePublisher(DEFAULT_DOMAIN_ID,
                                         BEST_EFFORT_PROFILE);
        }
        else
        {
            retVal = false;
        }

        DdsFactory::Destroy();

    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        retVal = false;
    }

    test_assert(retVal == true);

/* $end */
}

void _test_Entities_CreatePublisherFail(
    test_Entities _this)
{
/* $begin(test/Entities/CreatePublisherFail) */

    bool retVal = false;

    try
    {
        Initialize();
        dds::pub::Publisher publisher(dds::core::null);
        retVal = pDdsFactory->CreatePublisher(publisher,
                                              DEFAULT_DOMAIN_ID,
                                              BEST_EFFORT_PROFILE,
                                              BEST_EFFORT_BAD_FORMAT);

        if (retVal == true)
        {
            pDdsFactory->DeletePublisher(DEFAULT_DOMAIN_ID,
                                         BEST_EFFORT_PROFILE);
        }
        else
        {
            retVal = false;
        }

        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        retVal = true;
    }

    test_assert(retVal == true);
/* $end */
}

void _test_Entities_CreateSubscriber(
    test_Entities _this)
{
/* $begin(test/Entities/CreateSubscriber) */

    bool retVal = false;

    try
    {
        Initialize();
        dds::sub::Subscriber subscriber(dds::core::null);
        retVal = pDdsFactory->CreateSubscriber(subscriber,
                                              DEFAULT_DOMAIN_ID,
                                              BEST_EFFORT_PROFILE,
                                              BEST_EFFORT_PROFILE);

        if (retVal == true)
        {
            pDdsFactory->DeleteSubscriber(DEFAULT_DOMAIN_ID,
                                         BEST_EFFORT_PROFILE);
        }
        else
        {
            retVal = false;
        }

        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        retVal = false;
    }

    test_assert(retVal == true);

/* $end */
}

void _test_Entities_CreateSubscriberFail(
    test_Entities _this)
{
/* $begin(test/Entities/CreateSubscriberFail) */

    bool retVal = false;

    try
    {
        Initialize();
        dds::sub::Subscriber subscriber(dds::core::null);
        retVal = pDdsFactory->CreateSubscriber(subscriber,
                                              DEFAULT_DOMAIN_ID,
                                              BEST_EFFORT_BAD_FORMAT,
                                              BEST_EFFORT_PROFILE);

        if (retVal == true)
        {
            pDdsFactory->DeleteSubscriber(DEFAULT_DOMAIN_ID,
                                          BEST_EFFORT_PROFILE);
        }
        else
        {
            retVal = false;
        }

        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        retVal = true;
    }

    test_assert(retVal == true);
/* $end */
}
