/* $CORTO_GENERATED
 *
 * DomainParticipant.cpp
 *
 * Only code written between the begin and end tags will be preserved
 * when the file is regenerated.
 */

#include <include/test.h>

/* $header() */

/* $end */

void _test_DomainParticipant_Create(
    test_DomainParticipant _this)
{
/* $begin(test/DomainParticipant/Create) */

    bool retVal = false;

    try
    {
        CDdsFactory *pDdsFactory = DdsFactory::GetInstance();
        if (pDdsFactory != nullptr)
        {
            retVal = pDdsFactory->Initialize(QOS_URI);
        }

        if (retVal == true)
        {
            printf("\n\nOpen QOS_URI [%s]\n", QOS_URI.c_str());
            dds::domain::DomainParticipant participant(dds::core::null);
            retVal = pDdsFactory->CreateDomainParticipant(participant,
                                                          DEFAULT_DOMAIN_ID,
                                                          BEST_EFFORT_PROFILE);
        }
        else
        {
            printf("Failed to initialize DDS Factory.\n");
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]\n", e.what());
        test_assert(false);
    }
    catch (...)
    {
        printf("\n\nReceived unhandled exception\n\n");
        test_assert(false);
    }

    test_assert(retVal == true);

/* $end */
}

void _test_DomainParticipant_CreateBadProfile(
    test_DomainParticipant _this)
{
/* $begin(test/DomainParticipant/CreateBadProfile) */

    bool retVal = false;

    try
    {
        CDdsFactory *pDdsFactory = DdsFactory::GetInstance();
        if (pDdsFactory != nullptr)
        {
            retVal = pDdsFactory->Initialize(QOS_URI);
        }

        if (retVal == true)
        {
            printf("\n\nOpen QOS_URI [%s]\n", QOS_URI.c_str());
            dds::domain::DomainParticipant participant(dds::core::null);
            retVal = pDdsFactory->CreateDomainParticipant(participant,
                                                          DEFAULT_DOMAIN_ID,
                                                          BEST_EFFORT_BAD_FORMAT);
        }
        else
        {
            printf("Failed to initialize DDS Factory.\n");
        }
        DdsFactory::Destroy();
    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]", e.what());
        retVal = true;
    }

    test_assert(retVal == true);
/* $end */
}

void _test_DomainParticipant_CreateNoInit(
    test_DomainParticipant _this)
{
/* $begin(test/DomainParticipant/CreateNoInit) */

    bool retVal = false;

    try
    {
        CDdsFactory *pDdsFactory = DdsFactory::GetInstance();
        if (pDdsFactory != nullptr)
        {
            printf("\n\nOpen QOS_URI [%s]\n", QOS_URI.c_str());
            dds::domain::DomainParticipant participant(dds::core::null);
            retVal = pDdsFactory->CreateDomainParticipant(participant,
                                                          DEFAULT_DOMAIN_ID,
                                                          BEST_EFFORT_PROFILE);
        }
        DdsFactory::Destroy();

    }
    catch (OASYS_DDS_CONNEXT_EXCEPTION &e)
    {
        printf("Received exception [%s]", e.what());
        retVal = true;
    }

    test_assert(retVal == true);

/* $end */
}
