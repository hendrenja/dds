/* $CORTO_GENERATED
 *
 * Singleton.cpp
 *
 * Only code written between the begin and end tags will be preserved
 * when the file is regenerated.
 */

#include <include/test.h>

/* $header() */

class CTest
{
public:
    bool Destroy()
    {
        return true;
    }
};
typedef oasys::core::common::TSingleton<CTest> TestPtr;

/* $end */

void _test_Singleton_Allocate(
    test_Singleton _this)
{
/* $begin(test/Singleton/Allocate) */

    bool retVal = false;

    typedef oasys::core::common::TSingleton<int32_t> IntPtr;
    int32_t *pInt1 = IntPtr::GetInstance();
    if (pInt1 != nullptr)
    {
        int32_t *pInt2 = IntPtr::GetInstance();
        if ((pInt2 != nullptr) && (pInt1 == pInt2))
        {
            retVal = true;
        }
        else
        {
            test_assert(pInt2 != nullptr);
            test_assert(pInt1 == pInt2);
        }
    }
    else
    {
        test_assert(pInt1 != nullptr);
    }

    test_assert(retVal == TRUE);

/* $end */
}

void _test_Singleton_Delete(
    test_Singleton _this)
{
/* $begin(test/Singleton/Delete) */

    bool retVal = false;

    CTest *pTest = TestPtr::GetInstance();

    if (pTest != nullptr)
    {
        retVal = TestPtr::Destroy();
    }
    else
    {
        test_assert(pTest != nullptr);
    }

    test_assert(retVal == TRUE);

/* $end */
}

void _test_Singleton_NewPtr(
    test_Singleton _this)
{
/* $begin(test/Singleton/NewPtr) */

    bool retVal = false;

    CTest *pTest1 = TestPtr::GetInstance();

    if (pTest1 != nullptr)
    {
        TestPtr::Destroy();
    }
    else
    {
        test_assert(pTest1 != nullptr);
    }

    int *pInt = new int();
    CTest *pTest2 = TestPtr::GetInstance();

    if (pTest2 == nullptr)
    {
        test_assert(pTest2 != nullptr);
    }

    if (pTest1 != pTest2)
    {
        retVal = true;
    }

    delete pInt;
    test_assert(retVal == true);

/* $end */
}
