/* $CORTO_GENERATED
 *
 * Delegate.cpp
 *
 * Only code written between the begin and end tags will be preserved
 * when the file is regenerated.
 */

#include <include/test.h>

/* $header() */

typedef TDataDelegate<double> DataDelegate;
typedef TDataDelegate<int, double> DataDelegate2;

typedef CNotifyDelegate::Callback NotifyCallback;
typedef DataDelegate::Callback DataCallback;
typedef DataDelegate2::Callback DataCallback2;

/* $end */

void _test_Delegate_DelegateDead(
    test_Delegate _this)
{
/* $begin(test/Delegate/DelegateDead) */

    bool retVal = false;

    NotifyCallback callback = [&] (void)
    {
    };

    std::shared_ptr<int> pTest(new int(0));
    CNotifyDelegate delegate(callback, pTest);

    pTest.reset();
    if (delegate.Execute() == false)
    {
        if (delegate.Valid() == false)
        {
            retVal = true;
        }
    }

    test_assert(retVal);

/* $end */
}

void _test_Delegate_DelegateEqual(
    test_Delegate _this)
{
/* $begin(test/Delegate/DelegateEqual) */

    bool retVal = false;

    NotifyCallback callback = [&] (void)
    {
    };

    std::shared_ptr<int> pTest(new int(0));
    CNotifyDelegate delegate(callback, pTest);


    OwnerWeakPtr pOwner = delegate.GetOwnerPtr();
    if (delegate.Equal(pOwner) == true)
    {
        retVal = true;
    }

    test_assert(retVal);

/* $end */
}

void _test_Delegate_ExecuteArg0(
    test_Delegate _this)
{
/* $begin(test/Delegate/ExecuteArg0) */

    bool received = false;

    NotifyCallback callback = [&] (void)
    {
        received = true;
    };

    std::shared_ptr<int> pTest(new int(0));
    CNotifyDelegate delegate(callback, pTest);

    bool execute = delegate.Execute();

    bool retVal = execute && received;

    test_assert(retVal);
/* $end */
}

void _test_Delegate_ExecuteArg1(
    test_Delegate _this)
{
/* $begin(test/Delegate/ExecuteArg1) */

    bool received = false;
    double testDub = 332.55;
    DataCallback callback = [&] (double dub)
    {
        if (dub == 332.55)
        {
            received = true;
        }
    };

    std::shared_ptr<int> pTest(new int(0));
    DataDelegate delegate(callback, pTest);

    bool execute = delegate.Execute(testDub);

    bool retVal = execute && received;

    test_assert(retVal);

/* $end */
}

void _test_Delegate_ExecuteArg2(
    test_Delegate _this)
{
/* $begin(test/Delegate/ExecuteArg2) */

    bool received = false;
    double testDub = 332.55;
    int testNum = 3;
    DataCallback2 callback = [&] (int num, double dub)
    {
        if ((dub == 332.55) && (num == 3))
        {
            received = true;
        }
    };

    std::shared_ptr<int> pTest(new int(testNum));
    DataDelegate2 delegate(callback, pTest);

    bool execute = delegate.Execute(testNum, testDub);

    bool retVal = execute && received;

    test_assert(retVal == true);

/* $end */
}
