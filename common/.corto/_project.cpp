/* _project.cpp
 *
 * This file is generated. Do not modify.
 */

#include <common/common.h>
#include <common/_project.h>

int commonMain(int argc, char* argv[]);

int common_load(void);

#ifdef __cplusplus
extern "C"
#endif
COMMON_EXPORT int cortomain(int argc, char* argv[]) {
    if (corto_load("/corto/c", 0, NULL)) return -1;
    if (common_load()) return -1;
    if (commonMain(argc, argv)) return -1;
    return 0;
}

